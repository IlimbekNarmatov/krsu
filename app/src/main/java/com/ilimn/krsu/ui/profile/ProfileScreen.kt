@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.profile

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.ilimn.domain.entity.Citizenship
import com.ilimn.domain.entity.Profile
import com.ilimn.domain.entity.School
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.theme.outlinedTextColors
import com.ilimn.krsu.ui.utils.AppGradientButton
import com.ilimn.krsu.ui.utils.bottomBarHeight
import com.ilimn.krsu.ui.utils.painter
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.profile.ProfileVM
import kotlinx.coroutines.launch

@Composable
fun ProfileScreen(
    navController: NavController,
    vm: ProfileVM = viewModel(),
) {
    val coroutineScope = rememberCoroutineScope()
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = bottomBarHeight),
    ) {
        val modalSheetState: ModalBottomSheetState =
            rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
        ModalBottomSheetLayout(
            sheetContent = {
                CitizenshipBottomPage(
                    countries = vm.countries,
                    onSelected = {
                        vm.setCitizenship(it)
                        coroutineScope.launch {
                            modalSheetState.hide()
                        }
                    }
                )
            },
            sheetState = modalSheetState,
        ) {
            ProfilePage(
                firstName = vm.firstName,
                onFirstNameChange = { vm.setFirstName(it) },
                lastName = vm.lastName,
                onLastNameChange = { vm.setLastName(it) },
                citizenship = vm.citizenship,
                onCitizenshipClick = {
                    coroutineScope.launch {
                        modalSheetState.show()
                    }
                },
                school = vm.school,
                onSchoolChange = { vm.setSchool(School(it)) },
                onSaveClick = vm::update,
                scrollState = vm.scrollState,
                errors = vm.errors,
            )
        }
    }
}

@Preview
@Composable
fun ProfilePagePreview() {
    KRSUTheme {
        ProfilePage()
    }
}

@Composable
fun ProfilePage(
    firstName: State<String> = mutableStateOf(""),
    onFirstNameChange: (String) -> Unit = {},
    lastName: State<String> = mutableStateOf(""),
    onLastNameChange: (String) -> Unit = {},
    citizenship: State<Citizenship> = mutableStateOf(Citizenship.empty()),
    onCitizenshipClick: () -> Unit = {},
    school: State<School> = mutableStateOf(School.empty()),
    onSchoolChange: (String) -> Unit = {},
    scrollState: ScrollState = ScrollState(0),
    errors: Map<String, State<Boolean>> = emptyMap(),
    onSaveClick: () -> Unit = {},
) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
    ) {
        val (save, content) = createRefs()
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .constrainAs(content) {
                    top.linkTo(parent.top)
                }
        ) {
            Text(
                text = R.string.profile.string,
                style = MaterialTheme.typography.h5,
                modifier = Modifier.padding(top = 16.dp)
            )
            OutlinedTextField(
                value = firstName.value,
                onValueChange = onFirstNameChange,
                colors = MaterialTheme.colors.outlinedTextColors(),
                isError = errors[Profile.FIRST_NAME]?.value ?: false,
                label = {
                    Text(text = R.string.name.string)
                },
                modifier = Modifier
                    .padding(top = 20.dp)
                    .fillMaxWidth(),
            )
            OutlinedTextField(
                value = lastName.value,
                onValueChange = onLastNameChange,
                colors = MaterialTheme.colors.outlinedTextColors(),
                isError = errors[Profile.LAST_NAME]?.value ?: false,
                label = {
                    Text(text = R.string.last_name.string)
                },
                modifier = Modifier
                    .padding(top = 20.dp)
                    .fillMaxWidth(),
            )
            OutlinedTextField(
                value = citizenship.value.name,
                onValueChange = {},
                colors = MaterialTheme.colors.outlinedTextColors(),
                isError = errors[Profile.CITIZENSHIP]?.value
                    ?: false,
                label = {
                    Text(text = R.string.citizenship.string)
                },
                trailingIcon = {
                    IconButton(onClick = onCitizenshipClick) {
                        Icon(
                            painter = R.drawable.ic_arrow.painter,
                            contentDescription = R.string.open.string,
                            modifier = Modifier
                                .rotate(90f)
                        )
                    }
                },
                readOnly = true,
                modifier = Modifier
                    .padding(top = 16.dp)
                    .fillMaxWidth(),
            )
            OutlinedTextField(
                value = school.value.name,
                onValueChange = onSchoolChange,
                colors = MaterialTheme.colors.outlinedTextColors(),
                isError = errors[Profile.SCHOOL]?.value ?: false,
                label = {
                    Text(text = R.string.school.string)
                },
                modifier = Modifier
                    .padding(top = 16.dp)
                    .fillMaxWidth(),
            )
        }

        AppGradientButton(
            onClick = onSaveClick,
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(save) {
                    linkTo(
                        top = content.bottom,
                        bottom = parent.bottom,
                        bias = 1f,
                    )
                }
                .padding(vertical = 16.dp, horizontal = 16.dp)
        ) {
            Text(text = R.string.save.string)
        }
    }
}
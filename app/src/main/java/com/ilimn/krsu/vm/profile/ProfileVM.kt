package com.ilimn.krsu.vm.profile

import android.util.Log
import androidx.compose.foundation.ScrollState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.ilimn.domain.entity.Citizenship
import com.ilimn.domain.entity.Profile
import com.ilimn.domain.entity.School
import com.ilimn.domain.usecase.GetCountriesUC
import com.ilimn.domain.usecase.GetProfileUC
import com.ilimn.domain.usecase.UpdateProfileUC
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.vm.util.BaseVM
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class ProfileVM(
    private val updateProfileUC: UpdateProfileUC,
    private val getProfileUC: GetProfileUC,
    private val getCountriesUC: GetCountriesUC,
) : BaseVM() {

    private val _notification = MutableSharedFlow<Notification>()
    val notification: SharedFlow<Notification> = _notification
    val scrollState = ScrollState(0)
    private val _firstName = mutableStateOf("")
    val firstName: State<String> = _firstName
    private val _lastName = mutableStateOf("")
    val lastName: State<String> = _lastName
    private val _citizenship = mutableStateOf(Citizenship.empty())
    val citizenship: State<Citizenship> = _citizenship
    private val _school = mutableStateOf(School.empty())
    val school: State<School> = _school
    private val _errors =
        HashMap<String, MutableState<Boolean>>().apply {
            put(Profile.FIRST_NAME, mutableStateOf(false))
            put(Profile.LAST_NAME, mutableStateOf(false))
            put(Profile.CITIZENSHIP, mutableStateOf(false))
            put(Profile.SCHOOL, mutableStateOf(false))
        }
    val errors: Map<String, State<Boolean>> = _errors
    private val _countries =
        mutableStateOf<List<Citizenship>>(emptyList())
    val countries: State<List<Citizenship>> = _countries

    init {
        viewModelScope.launch {
            getProfileUC()
                .onSuccessSuspend {
                    _firstName.value = it.firstName
                    _lastName.value = it.lastName
                    _citizenship.value = it.citizenship
                    _school.value = it.school
                }
                .onFailureSuspend { _failure.send(it) }
        }
        viewModelScope.launch {
            getCountriesUC()
                .onSuccessSuspend { _countries.value = it }
                .onFailureSuspend { _failure.send(it) }
        }
    }

    fun setFirstName(firstName: String) {
        _firstName.value = firstName
        _errors[Profile.FIRST_NAME]?.value = false
    }

    fun setLastName(lastName: String) {
        _lastName.value = lastName
        _errors[Profile.LAST_NAME]?.value = false
    }

    fun setCitizenship(citizenship: Citizenship) {
        _citizenship.value = citizenship
        _errors[Profile.CITIZENSHIP]?.value = false
    }

    fun setSchool(school: School) {
        _school.value = school
        _errors[Profile.SCHOOL]?.value = false
    }

    fun update() {
        Log.println(Log.ASSERT, "ProfileVM", "update: ") // LOG delete
        viewModelScope.launch {
            updateProfileUC(
                Profile(
                    firstName = _firstName.value,
                    lastName = _lastName.value,
                    citizenship = _citizenship.value,
                    school = _school.value
                )
            ).onSuccessSuspend {
                Log.println(
                    Log.ASSERT,
                    "ProfileVM",
                    "update: success"
                ) // LOG delete
                _notification.emit(Notification.SuccessUpdate)
            }.onFailureSuspend {
                if (it is Failure.Format) {
                    Log.println(
                        Log.ASSERT,
                        "ProfileVM",
                        "update: $it"
                    ) // LOG delete
                    _errors.keys.forEach { key ->
                        _errors[key]?.value = it.type.contains(key)
                    }
                } else {
                    _failure.send(it)
                }
            }
        }
    }

    sealed class Notification {
        object SuccessUpdate : Notification()
    }

}
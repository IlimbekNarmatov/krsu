package com.ilimn.domain.entity

data class OnBoarding(
    val id: Int,
    val title: String,
    val description: String,
    val images: List<String>
)

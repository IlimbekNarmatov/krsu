package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class GetFaqsUC(
    private val repo: AppRepo
) {

    operator fun invoke(
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, Any> = repo.getFaqs()

}

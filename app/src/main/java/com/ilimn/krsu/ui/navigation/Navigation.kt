package com.ilimn.krsu.ui.navigation

import android.os.Bundle
import androidx.navigation.*
import androidx.navigation.compose.composable
import com.ilimn.krsu.ui.auth.ConfirmScreen
import com.ilimn.krsu.ui.auth.LoginScreen
import com.ilimn.krsu.ui.auth.RegistrationScreen
import com.ilimn.krsu.ui.faq.AskQuestionScreen
import com.ilimn.krsu.ui.faq.FaqScreen
import com.ilimn.krsu.ui.main.ApplyScreen
import com.ilimn.krsu.ui.main.MainScreen
import com.ilimn.krsu.ui.main.ProfessionScreen
import com.ilimn.krsu.ui.profile.ProfileScreen
import com.ilimn.krsu.ui.start.OnBoardingScreen
import com.ilimn.krsu.ui.start.SplashScreen
import com.ilimn.krsu.vm.main.MainVM
import com.ilimn.krsu.vm.start.OnBoardingVM
import com.ilimn.krsu.vm.start.SplashVM
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.compose.viewModel
import org.koin.core.parameter.parametersOf

fun getNavigation() {
}

fun NavGraphBuilder.graph(navController: NavController) {
    composable(route = SPLASH_SCREEN) {
        val vm by viewModel<SplashVM>()
        SplashScreen(
            navController,
            vm
        )
    }
    composable(route = ON_BOARDING_SCREEN) {
        val vm by viewModel<OnBoardingVM>()
        OnBoardingScreen(
            navController,
            vm
        )
    }
    composable(route = MAIN_SCREEN) {
        val vm by viewModel<MainVM>()
        MainScreen(
            navController,
            vm
        )
    }
    composable(
        route = PROFESSION_SCREEN, arguments = listOf(
            navArgument("id") { type = NavType.IntType }
        )
    ) {
        it.arguments?.getInt("id")?.let { id ->
            ProfessionScreen(
                navController,
                id,
                getViewModel()
            )
        }
    }
    composable(route = APPLY_SCREEN) { ApplyScreen(navController) }
    composable(route = FAQ_SCREEN) {
        FaqScreen(
            navController,
            getViewModel()
        )
    }
    composable(route = ASQ_QUESTION_SCREEN) {
        AskQuestionScreen(
            navController,
            getViewModel(),
        )
    }
    composable(route = PROFILE_SCREEN) {
        ProfileScreen(
            navController,
            getViewModel()
        )
    }
    composable(route = LOGIN_SCREEN) {
        LoginScreen(navController, getViewModel())
    }
    composable(route = REGISTRATION_SCREEN) {
        RegistrationScreen(
            navController,
            getViewModel(),
        )
    }
    composable(
        route = CONFIRM_SCREEN, arguments = listOf(
            navArgument(ARG_EMAIL) { type = NavType.StringType }
        )
    ) {
        it.arguments?.getString(ARG_EMAIL)?.let { email ->
            ConfirmScreen(
                navController,
                email,
                getViewModel(parameters = {
                    val arguments = Bundle().apply {
                        putString(ARG_EMAIL, email)
                    }
                    parametersOf(arguments)
                })
            )
        }
    }
}

fun NavGraphBuilder.startGraph(navController: NavController) {
    navigation(
        startDestination = SPLASH_SCREEN,
        route = START_GRAPH
    ) {
        composable(route = SPLASH_SCREEN) {
            val vm by viewModel<SplashVM>()
            SplashScreen(
                navController,
                vm
            )
        }
        composable(route = ON_BOARDING_SCREEN) {
            val vm by viewModel<OnBoardingVM>()
            OnBoardingScreen(
                navController,
                vm
            )
        }
    }
}

fun NavGraphBuilder.mainGraph(navController: NavController) {
    navigation(startDestination = MAIN_SCREEN, route = MAIN_GRAPH) {
        composable(route = MAIN_SCREEN) { MainScreen(navController) }
        composable(route = PROFESSION_SCREEN) {
            ProfessionScreen(
                navController,
                0
            )
        }
        composable(route = APPLY_SCREEN) { ApplyScreen(navController) }
    }
}

fun NavGraphBuilder.faqGraph(navController: NavController) {
    navigation(startDestination = FAQ_SCREEN, route = FAQ_GRAPH) {
        composable(route = FAQ_SCREEN) { FaqScreen(navController) }
        composable(route = ASQ_QUESTION_SCREEN) {
            AskQuestionScreen(
                navController
            )
        }
    }
}

fun NavGraphBuilder.profileGraph(navController: NavController) {
    navigation(
        startDestination = PROFILE_SCREEN,
        route = PROFILE_GRAPH
    ) {
        composable(route = PROFILE_SCREEN) {
            ProfileScreen(
                navController,
            )
        }
    }
}

fun NavGraphBuilder.loginGraph(navController: NavController) {
    navigation(startDestination = LOGIN_SCREEN, route = LOGIN_GRAPH) {
        composable(route = LOGIN_SCREEN) { LoginScreen(navController) }
        composable(route = REGISTRATION_SCREEN) {
            RegistrationScreen(
                navController
            )
        }
        composable(route = CONFIRM_SCREEN) {
            ConfirmScreen(
                navController,
                ""
            )
        }
    }
}
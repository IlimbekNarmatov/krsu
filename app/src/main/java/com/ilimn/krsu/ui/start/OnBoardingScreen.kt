@file:OptIn(
    ExperimentalComposeUiApi::class,
    ExperimentalComposeUiApi::class, ExperimentalPagerApi::class,
    ExperimentalMaterialApi::class, ExperimentalPagerApi::class
)

package com.ilimn.krsu.ui.start

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.google.accompanist.pager.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.ilimn.domain.entity.OnBoarding
import com.ilimn.domain.util.safeGet
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.compasable.BackArrow
import com.ilimn.krsu.ui.navigation.ON_BOARDING_SCREEN
import com.ilimn.krsu.ui.navigation.SPLASH_SCREEN
import com.ilimn.krsu.ui.navigation.START_GRAPH
import com.ilimn.krsu.ui.navigation.mainGraph
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.theme.textPrimaryColor
import com.ilimn.krsu.ui.theme.textSecondaryColor
import com.ilimn.krsu.ui.utils.AppButton
import com.ilimn.krsu.ui.utils.navigatePopUp
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.start.OnBoardingVM
import com.ilimn.krsu.vm.start.SplashVM
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel

@ExperimentalComposeUiApi
@Preview
@Composable
fun OnBoardingPreview() {
    KRSUTheme {
        OnBoardingScreen(navController = rememberNavController())
    }
}

@ExperimentalComposeUiApi
@Preview
@Composable
fun OnBoardingDeployPreview() {
    KRSUTheme {
        val navController = rememberNavController()
        NavHost(
            navController = navController,
            startDestination = START_GRAPH
        ) {
            navigation(
                startDestination = ON_BOARDING_SCREEN,
                route = START_GRAPH
            ) {
                composable(route = SPLASH_SCREEN) {
                    val vm = getViewModel<SplashVM>()
                    SplashScreen(
                        navController,
                        vm
                    )
                }
                composable(route = ON_BOARDING_SCREEN) {
                    val vm = getViewModel<OnBoardingVM>()
                    OnBoardingScreen(
                        navController,
                        vm
                    )
                }
                mainGraph(navController = navController)
            }
        }
    }
}

@Composable
fun OnBoardingScreen(
    navController: NavController,
    vm: OnBoardingVM = viewModel()
) {
    rememberSystemUiController().apply {
        setStatusBarColor(MaterialTheme.colors.background)
    }
    LaunchedEffect(Unit) {
        vm.navigation
            .collect { navigation ->
                navController.navigatePopUp(navigation)
            }
    }
    val coroutine = rememberCoroutineScope()
    val onBoardings = vm.onBoardings.collectAsState()
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {
        var isToolbarVisible by remember {
            mutableStateOf(false)
        }
        var isButtonVisible by remember {
            mutableStateOf(false)
        }
        val (backArrow, content,
            indicator, nextButton
        ) = createRefs()
        val pagerState = rememberPagerState()
        OnBoardingPagerScreen(
            modifier = Modifier
                .constrainAs(content) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
                .padding(bottom = 70.dp)
                .fillMaxSize(),
            onBoardings = onBoardings.value,
            page = 0,
            pagerState = pagerState,
        ) { page ->
            isToolbarVisible = page != 0
            isButtonVisible = page == onBoardings.value.size - 1
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
                .height(46.dp)
                .constrainAs(indicator) {
                    bottom.linkTo(parent.bottom)
                },
            contentAlignment = Alignment.Center
        ) {
            AnimatedVisibility(
                enter = fadeIn(),
                exit = fadeOut(),
                visible = !isButtonVisible,
            ) {
                HorizontalPagerIndicator(
                    pagerState = pagerState,
                    activeColor = MaterialTheme.colors.primary,
                    modifier = Modifier
                )
            }
        }
        AnimatedVisibility(
            visible = isToolbarVisible,
            enter = fadeIn(),
            exit = fadeOut(),
            modifier = Modifier
                .constrainAs(backArrow) {
                    top.linkTo(parent.top, margin = 20.dp)
                    start.linkTo(parent.start, margin = 12.dp)
                },
        ) {
            BackArrow(
                modifier = Modifier
            ) {
                coroutine.launch {
                    pagerState.apply {
                        animateScrollToPage(currentPage - 1)
                    }
                }
            }
        }
        AnimatedVisibility(
            visible = isButtonVisible,
            enter = fadeIn(),
            exit = fadeOut(),
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .constrainAs(nextButton) {
                    bottom.linkTo(parent.bottom, margin = 16.dp)
                },
        ) {
            AppButton(
                onClick = { vm.onBoardingSeen() },
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(text = R.string.begin.string)
            }
        }
    }
}

@Composable
private fun OnBoardingPagerScreen(
    modifier: Modifier = Modifier,
    onBoardings: List<OnBoarding>,
    page: Int,
    pagerState: PagerState = rememberPagerState(),
    onPageChangeCallback: (Int) -> Unit,
) {
    LaunchedEffect(pagerState) {
        snapshotFlow {
            pagerState.currentPage
        }.collect { page ->
            onPageChangeCallback(page)
        }
    }
    HorizontalPager(
        count = onBoardings.size, state = pagerState,
        modifier = modifier
    ) { index ->
        OnBoardingPage(onBoarding = onBoardings[index])
    }
    LaunchedEffect(page) {
        pagerState.scrollToPage(page)
    }
}

@Preview
@Composable
fun OnBoardingPagePreview() {
    val onBoarding = OnBoarding(
        0,
        "Lorem ipsum",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
        listOf("https://picsum.photos/id/1/200/200")
    )
    OnBoardingPage(onBoarding = onBoarding)
}

@Composable
private fun OnBoardingPage(
    modifier: Modifier = Modifier,
    onBoarding: OnBoarding
) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .then(modifier)
    ) {
        val (
            imageCS, titleCS,
            title2CS, descriptionCS, buttonCS
        ) = createRefs()
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(onBoarding.images.firstOrNull())
                .crossfade(true)
                .build(),
            contentScale = ContentScale.Fit,
            contentDescription = R.string.on_boarding.string,
            modifier = Modifier
                .constrainAs(imageCS) {
                    top.linkTo(parent.top)
                }
                .fillMaxWidth()
                .clip(
                    shape = RoundedCornerShape(
                        bottomEnd = 10.dp,
                        bottomStart = 10.dp
                    )
                )
        )
        val text = onBoarding.title.split("\n")
        val title = text.safeGet(0) ?: ""
        val title2 = text.safeGet(1)
        Text(
            text = title,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .constrainAs(titleCS) {
                    top.linkTo(
                        anchor = imageCS.bottom,
                        margin = 24.dp
                    )
                },
            style = MaterialTheme.typography.h5,
            color = MaterialTheme.colors.textPrimaryColor(),
        )
        if (title2 != null) {
            Text(
                text = title2,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .constrainAs(title2CS) {
                        top.linkTo(anchor = titleCS.bottom)
                    },
                style = MaterialTheme.typography.body1,
                color = MaterialTheme.colors.textSecondaryColor(),
            )
        }
        Text(
            text = onBoarding.description,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .constrainAs(descriptionCS) {
                    val bottom =
                        if (title2 != null) title2CS.bottom
                        else titleCS.bottom
                    top.linkTo(
                        anchor = bottom,
                        margin = 20.dp
                    )
                },
            style = MaterialTheme.typography.caption,
            color = MaterialTheme.colors.textSecondaryColor(),
        )
    }
}
package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class GetIsOnBoardingSeenUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, Boolean>, BaseUseCase.None>() {

    override suspend fun run(params: None): Either<Failure, Boolean> =
        repo.isOnBoardingSeen()

}
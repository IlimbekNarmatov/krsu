package com.ilimn.krsu.ui.navigation

const val uri = "https://krsu.kg"

const val ARG_EMAIL = "email"
const val ARG_PASSWORD = "password"

const val LOGIN_GRAPH = "$uri/graph/login"
const val LOGIN_SCREEN = "$uri/login"
const val REGISTRATION_SCREEN = "$uri/registration"
const val CONFIRM_SCREEN = "$uri/confirm/{$ARG_EMAIL}"
const val CONFIRM_SCREEN_NO_ARG = "$uri/confirm/"

const val MAIN_GRAPH = "$uri/graph/main"
const val MAIN_SCREEN = "$uri/main"
const val PROFESSION_SCREEN = "$uri/profession/{id}"
const val PROFESSION_SCREEN_NO_ARG = "$uri/profession/"
const val APPLY_SCREEN = "$uri/apply"

const val START_GRAPH = "$uri/graph/start"
const val SPLASH_SCREEN = "$uri/splash"
const val ON_BOARDING_SCREEN = "$uri/onBoarding"

const val FAQ_GRAPH = "$uri/graph/faq"
const val FAQ_SCREEN = "$uri/faq"
const val ASQ_QUESTION_SCREEN = "$uri/asqQuestion"

const val PROFILE_GRAPH = "$uri/graph/profile"
const val PROFILE_SCREEN = "$uri/profile"
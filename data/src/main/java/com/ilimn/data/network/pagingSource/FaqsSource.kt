package com.ilimn.data.network.pagingSource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ilimn.data.network.entities.FaqNO
import com.ilimn.data.network.entities.PageNO
import com.ilimn.data.network.entities.toFaq
import com.ilimn.data.util.getException
import com.ilimn.domain.entity.Faq
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

internal class FaqsSource(
    private val fn: suspend (page: Int) -> Either<Failure, PageNO<FaqNO>>
) : PagingSource<Int, Faq>() {

    override fun getRefreshKey(
        state: PagingState<Int, Faq>
    ): Int? {
        return state.anchorPosition
    }

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Faq> =
        when (val response = fn(params.key ?: 1)) {
            is Either.Left -> {
                LoadResult.Error(response.l.getException())
            }
            is Either.Right -> {
                LoadResult.Page(
                    data = response.r.results.map {
                        it.toFaq()
                    },
                    prevKey = null,
                    nextKey = response.r.next
                )
            }
        }
}
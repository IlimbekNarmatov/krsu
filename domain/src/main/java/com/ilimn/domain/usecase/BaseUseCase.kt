package com.ilimn.domain.usecase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

abstract class BaseUseCase<out Type : Any?, in Params> {

    suspend operator fun invoke(
        params: Params
    ): Type = coroutineScope {
        val deferred = async(Dispatchers.IO) {
            run(params)
        }
        deferred.await()
    }

    abstract suspend fun run(params: Params): Type

    object None
}
package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.Success

class OnBoardingSeenUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, Success>, BaseUseCase.None>() {

    override suspend fun run(params: None): Either<Failure, Success> =
        repo.onBoardingSeen()

}
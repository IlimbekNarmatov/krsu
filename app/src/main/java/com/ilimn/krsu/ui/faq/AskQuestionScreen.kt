@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.faq

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemsIndexed
import com.ilimn.domain.entity.Message
import com.ilimn.domain.entity.MyMessage
import com.ilimn.domain.util.safeGet
import com.ilimn.krsu.R.string
import com.ilimn.krsu.ui.compasable.*
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.MessageTextField
import com.ilimn.krsu.ui.utils.SendIcon
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.faq.AskQuestionVM
import org.koin.androidx.compose.getViewModel

@Preview
@Composable
fun AsqQuestionScreenPreview() {
    KRSUTheme {
        AskQuestionScreen(
            navController = rememberNavController(),
            getViewModel(),
        )
    }
}

@Composable
fun AskQuestionScreen(
    navController: NavController,
    vm: AskQuestionVM = viewModel(),
) {
    val messages =
        vm.getMessagesPagination()?.collectAsLazyPagingItems()
    LaunchedEffect(Unit) {
        vm.action.collect { action ->
            if (action is AskQuestionVM.Action.Refresh) {
                messages?.refresh()
            }
        }
    }
    Column(modifier = Modifier.fillMaxSize()) {
        Toolbar(text = string.support.string) {
            navController.navigateUp()
        }
        Box(modifier = Modifier.weight(1f)) {
            messages?.let {
                MessagesList(
                    messages = it,
                    scrollState = vm.messagesScrollState,
                    modifier = Modifier.fillMaxSize(),
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colors.primary.copy(alpha = 0.05f)),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            MessageTextField(
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 10.dp, vertical = 8.dp),
                value = vm.message.value,
                onValueChange = {
                    vm.setMessage(it)
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedBorderColor = Color.Transparent,
                    backgroundColor = MaterialTheme.colors.onPrimary,
                )
            )
            SendIcon(modifier = Modifier.padding(end = 6.dp)) {
                vm.sendMessage()
            }
        }
    }
}

@Composable
private fun MessagesList(
    modifier: Modifier = Modifier,
    messages: LazyPagingItems<Message>,
    scrollState: LazyListState = LazyListState(),
) {
    LazyColumn(
        modifier = modifier,
        reverseLayout = true,
        state = scrollState,
    ) {
        itemsIndexed(messages) { i, message ->
            message?.let {
                val nextMessage =
                    messages.itemSnapshotList.safeGet(i - 1)
                val isNextMyMessage =
                    nextMessage?.let { nextMessage ->
                        nextMessage.messageType is MyMessage
                    }
                if (it.messageType is MyMessage) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                end = 23.dp,
                                start = if (isNextMyMessage == true) 85.dp else 42.dp,
                                bottom = if (isNextMyMessage == true) 8.dp else 20.dp
                            ),
                        contentAlignment = Alignment.CenterEnd
                    ) {
                        if (isNextMyMessage == true) {
                            MyTextMessage(message = it)
                        } else {
                            MyLastTextMessage(message = it)
                        }
                    }
                } else {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                end = if (isNextMyMessage == false) 85.dp else 42.dp,
                                start = 23.dp,
                                bottom = if (isNextMyMessage == false) 8.dp else 20.dp,
                            ),
                        contentAlignment = Alignment.CenterStart
                    ) {
                        if (isNextMyMessage == false) {
                            CompanionTextMessage(message = it)
                        } else {
                            CompanionLastTextMessage(message = it)
                        }
                    }
                }
            }
        }
    }
}
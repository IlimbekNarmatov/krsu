package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Version
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.usecase.BaseUseCase.None
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class GetVersionUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, Version>, None>() {

    override suspend fun run(params: None): Either<Failure, Version> =
        repo.getVersion()

}

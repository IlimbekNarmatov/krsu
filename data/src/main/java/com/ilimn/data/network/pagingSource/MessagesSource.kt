package com.ilimn.data.network.pagingSource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ilimn.data.network.entities.MessageNO
import com.ilimn.data.network.entities.PageNO
import com.ilimn.data.network.entities.toMessage
import com.ilimn.data.util.getException
import com.ilimn.domain.entity.Message
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

internal class MessagesSource(
    private val fn: suspend (page: Int) -> Either<Failure, PageNO<MessageNO>>
) : PagingSource<Int, Message>() {

    override fun getRefreshKey(
        state: PagingState<Int, Message>
    ): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(
                1
            )
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(
                    1
                )
        }
    }

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Message> =
        when (val response = fn(params.key ?: 0)) {
            is Either.Left -> {
                LoadResult.Error(response.l.getException())
            }
            is Either.Right -> {
                LoadResult.Page(
                    data = response.r.results.map {
                        it.toMessage()
                    },
                    prevKey = response.r.previous,
                    nextKey = response.r.next
                )
            }
        }
}
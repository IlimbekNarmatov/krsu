package com.ilimn.krsu.vm.util

class OneShot<T>(private val content: T) {
    var hasBeenHandled = false
        private set

    fun unhandledOr(default: T? = null): T? =
        if (hasBeenHandled) {
            default
        } else {
            hasBeenHandled = true
            content
        }

    fun peekContent(): T = content
}

fun <T> T.toShot() = OneShot(this)
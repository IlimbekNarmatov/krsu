package com.ilimn.domain.entity

data class Faq(
    val id: Int,
    val question: String,
    val answer: String
)

package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class GetMessagesPagingUC(
    private val repo: AppRepo
) {

    operator fun invoke(
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Any = repo.getMessagesPaging()

}
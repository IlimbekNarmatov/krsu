package com.ilimn.krsu.vm.util

import androidx.lifecycle.ViewModel
import com.ilimn.domain.util.Failure
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow

open class BaseVM : ViewModel() {

    protected val _loading = Channel<Boolean>(capacity = 1)
    val loading = _loading.receiveAsFlow()
    protected val _failure = Channel<Failure>(capacity = 1)
    val failure = _failure.receiveAsFlow()

}
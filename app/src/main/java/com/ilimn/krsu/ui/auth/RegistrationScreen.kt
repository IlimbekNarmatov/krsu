@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.auth

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.compasable.BackArrow
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.theme.outlinedTextColors
import com.ilimn.krsu.ui.utils.*
import com.ilimn.krsu.vm.auth.RegistrationVM

@Composable
fun RegistrationScreen(
    navController: NavController,
    vm: RegistrationVM = viewModel()
) {
    LaunchedEffect(Unit) {
        vm.navigation.collect { navigation ->
            navController.navigatePopUp(navigation)
        }
    }
    RegistrationPage(
        onEmailChange = { vm.setEmail(it) },
        onPasswordChange = { vm.setPassword(it) },
        emailError = vm.emailError,
        passwordError = vm.passwordError,
        onSignUpClick = { vm.signUp() },
        email = vm.email,
        password = vm.password,
        onBackClick = { navController.navigateUp() },
        scrollState = vm.pageScrollState,
    )
}

@Preview
@Composable
fun RegistrationPagePreview() {
    KRSUTheme {
        RegistrationPage()
    }
}

@Composable
fun RegistrationPage(
    onEmailChange: (String) -> Unit = {},
    onPasswordChange: (String) -> Unit = {},
    emailError: State<Boolean> = mutableStateOf(false),
    passwordError: State<Boolean> = mutableStateOf(false),
    onSignUpClick: () -> Unit = {},
    email: State<String> = mutableStateOf(""),
    password: State<String> = mutableStateOf(""),
    onBackClick: () -> Unit = {},
    scrollState: ScrollState = ScrollState(0),
) {
    Column(
        modifier = Modifier
            .fillMaxSize(),
    ) {
        BackArrow(
            modifier = Modifier.padding(
                top = 16.dp,
                start = 12.dp
            ),
            onClick = { onBackClick() },
        )
        Row(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
                .padding(top = 8.dp),
        ) {
            Spacer(modifier = Modifier.width(16.dp))
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .verticalScroll(scrollState),
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = R.string.registration.string,
                    style = MaterialTheme.typography.h5,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onBackground,
                    modifier = Modifier
                        .fillMaxWidth(),
                )
                Text(
                    text = R.string.registration_with_email.string,
                    style = MaterialTheme.typography.body2,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onBackground.copy(
                        0.4f
                    ),
                    modifier = Modifier
                        .fillMaxWidth(),
                )
                Spacer(modifier = Modifier.height(20.dp))
                val colors = MaterialTheme.colors.outlinedTextColors()
                Email(
                    modifier = Modifier.fillMaxWidth(),
                    email = email,
                    onChange = onEmailChange,
                    emailError = emailError,
                    colors = colors,
                    isErrorTextVisible = true,
                    errorContent = {
                        Text(text = R.string.email_format_error.string)
                    }
                )
                Password(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 2.dp),
                    password = password,
                    onChange = onPasswordChange,
                    passwordError = passwordError,
                    colors = colors,
                    isErrorTextVisible = true,
                    errorContent = {
                        Text(text = R.string.password_format_error.string)
                    }
                )
                AppGradientButton(
                    modifier = Modifier
                        .padding(top = 12.dp)
                        .fillMaxWidth(),
                    onClick = { onSignUpClick() }
                ) {
                    Text(text = R.string.sign_up.string)
                }
            }
            Spacer(modifier = Modifier.width(16.dp))
        }
    }
}

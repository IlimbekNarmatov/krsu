@file:OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalMaterialApi::class
)

package com.ilimn.krsu.ui.main

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.ilimn.domain.entity.ApplyingDescription
import com.ilimn.domain.util.toAnswer
import com.ilimn.domain.util.toQuestion
import com.ilimn.domain.util.toggle
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.compasable.Toolbar
import com.ilimn.krsu.ui.theme.textPrimaryColor
import com.ilimn.krsu.ui.theme.textSecondaryColor
import com.ilimn.krsu.ui.utils.AppButton
import com.ilimn.krsu.ui.utils.AppProgress
import com.ilimn.krsu.ui.utils.ExpandableTextCard
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.main.ProfessionVM

@Composable
fun ProfessionScreen(
    navController: androidx.navigation.NavController,
    id: Int,
    vm: ProfessionVM = viewModel(),
) {
    LaunchedEffect(Unit) {
        vm.getDetailProfession(id)
    }
    val profession = vm.detailProfession.collectAsState()
    var isApplying by remember {
        mutableStateOf(false)
    }
    val expandedSet = remember {
        mutableStateOf<Set<Int>>(emptySet())
    }
    val toggleExpand = { item: ApplyingDescription ->
        expandedSet.value = HashSet<Int>().apply {
            addAll(expandedSet.value)
            toggle(item.id)
        }
    }
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Bottom,
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
        ) {
            item {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                ) {
                    Spacer(modifier = Modifier.height(16.dp))
                    Toolbar { navController.navigateUp() }
                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = profession.value?.title ?: "",
                        style = MaterialTheme.typography.h5,
                        fontWeight = FontWeight.Bold,
                        color = MaterialTheme.colors.textPrimaryColor(),
                        textAlign = TextAlign.Start,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                    )
                    Text(
                        text = profession.value?.title2 ?: "",
                        style = MaterialTheme.typography.caption,
                        color = MaterialTheme.colors.textPrimaryColor(),
                        textAlign = TextAlign.Start,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                    )
                }
            }
            item {
                if (profession.value?.images?.isNotEmpty() == true) {
                    ProfessionImages(
                        state = vm.imagesState,
                        images = getProfessionDumpImages(
                            profession.value?.id ?: 0
                        )
                    )
                }
            }
            item {
                Spacer(modifier = Modifier.height(22.dp))
                Text(
                    text = profession.value?.description ?: "",
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.textSecondaryColor(),
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                )
                Spacer(modifier = Modifier.height(20.dp))
            }
            if (isApplying) {
                items(profession.value?.applying ?: emptyList()) {
                    ExpandableTextCard(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        question = it.title.toQuestion(),
                        answer = it.description.toAnswer(),
                        isAnswerExpanded = expandedSet.value.contains(
                            it.id
                        ),
                        onQuestionClick = { question ->
                            toggleExpand(
                                it
                            )
                        },
                        onAnswerClick = { answer ->
                            toggleExpand(
                                it
                            )
                        },
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                }
            }
        }
        Column {
            Spacer(modifier = Modifier.height(16.dp))
            AppButton(
                onClick = { isApplying = !isApplying },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
            ) {
                Text(text = R.string.apply_rules.string)
            }
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
    if (profession.value == null) {
        AppProgress()
    }
}

@Composable
private fun ProfessionImages(
    state: LazyListState,
    images: List<Int>,
) {
    LazyRow(
        modifier = Modifier
            .fillMaxWidth()
            .height(189.dp)
            .padding(vertical = 2.dp),
        state = state,
    ) {
        itemsIndexed(images) { index, image ->
            Spacer(modifier = Modifier.width(16.dp))
            AsyncImage(
                model = image,
                contentScale = ContentScale.Crop,
                contentDescription = R.string.profession_image.string,
                modifier = Modifier
                    .width(296.dp)
                    .height(185.dp)
                    .clip(shape = RoundedCornerShape(8.dp))
            )
            if (index == images.size - 1) {
                Spacer(modifier = Modifier.width(16.dp))
            }
        }
    }
}

private fun getProfessionDumpImages(id: Int): List<Int> {
    return when (id) {
        1 -> listOf(
            R.drawable.example_image_14,
            R.drawable.example_image_1,
            R.drawable.example_image_2,
            R.drawable.example_image_3,
            R.drawable.example_image_4,
        )
        8 -> listOf(
            R.drawable.example_image_5,
            R.drawable.example_image_6,
        )
        4 -> listOf(
            R.drawable.example_image_7,
            R.drawable.example_image_8,
            R.drawable.example_image_9,
            R.drawable.example_image_10,
        )
        3 -> listOf(
            R.drawable.example_image_11,
            R.drawable.example_image_12,
            R.drawable.example_image_13,
        )
        7 -> listOf(
            R.drawable.example_image_15,
            R.drawable.example_image_16,
        )
        9 -> listOf(
            R.drawable.example_image_17,
            R.drawable.example_image_18,
            R.drawable.example_image_19,
        )
        else -> emptyList()
    }
}
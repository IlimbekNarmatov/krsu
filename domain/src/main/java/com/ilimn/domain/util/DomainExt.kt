package com.ilimn.domain.util

import com.ilimn.domain.entity.Answer
import com.ilimn.domain.entity.Question

fun <T> List<T>.safeGet(index: Int): T? =
    if (index < 0 || size <= index) null
    else get(index)

fun <T> List<T>.safeSubList(from: Int, to: Int): List<T> =
    when {
        from < 0 || to < 0 -> emptyList()
        to < from -> emptyList()
        to >= size -> ArrayList(this)
        else -> subList(from, to)
    }

fun <T> List<T>.toQuadruple(default: T): Quadruple<T, T, T, T> {
    val list = MutableList<T>(4) {
        if (size > it) this[it]
        else default
    }
    return list.toQuadrupleForce()
}

fun <T> List<T>.toQuadrupleForce() = Quadruple(
    get(0), get(1), get(2), get(3)
)

fun <T> tryCatch(fn: () -> T?): T? =
    try {
        fn()
    } catch (e: Exception) {
        null
    }

fun getNetworkFailure(
    code: Int,
    message: String?
): NewFailure.Network = when (code) {
    in 500 until 600 -> NewFailure.Network.Server
    400 -> NewFailure.Network.BadRequest(message)
    401 -> NewFailure.Network.Unauthorized
    403 -> NewFailure.Network.Forbidden
    404 -> NewFailure.Network.NotFound
    413 -> NewFailure.Network.BigEntity
    -1 -> NewFailure.Network.NoConnection
    else -> NewFailure.Network.Default(code)
}

fun <T> List<T>.safeLast(): T? = safeGet(this.size - 1)

inline fun <reified T> tryCast(instance: Any?, block: (T) -> Unit) {
    if (instance is T) block(instance)
}

fun String.toAnswer() = Answer(this)

fun String.toQuestion() = Question(this)

fun <T> MutableSet<T>.toggle(element: T) {
    if (!add(element)) {
        remove(element)
    }
}
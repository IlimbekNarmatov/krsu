package com.ilimn.domain.util

sealed class Failure {

    sealed class Network : Failure() {
        object Unauthorized : Network()
        object NotFound : Network()
        object NoConnection : Network()
        object Default : Network()
    }

    data class Server(
        val code: Int, val message: String? = null
    ) : Failure()

    data class Util(val message: String) : Failure()

    object Parse : Failure()

    object Default : Failure()

    data class Format(val type: Set<String>) : Failure()
}
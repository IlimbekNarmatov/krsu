package com.ilimn.domain.repository

import com.ilimn.domain.entity.*
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.Success

interface AppRepo {
    suspend fun getToken(): Either<Failure, Token>
    suspend fun setToken(token: Token): Either<Failure, Success>
    suspend fun getVersion(): Either<Failure, Version>
    suspend fun isOnBoardingSeen(): Either<Failure, Boolean>
    suspend fun onBoardingSeen(): Either<Failure, Success>
    suspend fun isLoggedIn(): Either<Failure, Boolean>
    fun getProfessions(): Either<Failure, Any>
    suspend fun searchProfession(keyword: String): Either<Failure, List<Profession>>
    suspend fun getDetailProfession(id: Int): Either<Failure, DetailProfession>
    suspend fun getApplying(id: Int): Either<Failure, Applying>
    fun getFaqs(): Either<Failure, Any>
    suspend fun signIn(login: Login): Either<Failure, Token>
    suspend fun signUp(login: Login): Either<Failure, Success>
    suspend fun confirm(confirm: Confirm): Either<Failure, Token>
    suspend fun sendMessage(message: Message): Either<Failure, Success>
    fun getMessagesPaging(): Any
    suspend fun getProfile(): Either<Failure, Profile>
    suspend fun getOnBoardings(): Either<Failure, List<OnBoarding>>
    suspend fun updateProfile(profile: Profile): Either<Failure, Profile>
    suspend fun getCountries(): Either<Failure, List<Citizenship>>
    suspend fun getMessages(
        page: Int,
        size: Int
    ): Either<Failure, List<Message>>
}
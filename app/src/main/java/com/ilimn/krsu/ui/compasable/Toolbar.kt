package com.ilimn.krsu.ui.compasable

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.theme.KRSUTheme

@ExperimentalMaterialApi
@Preview
@Composable
fun ToolbarPreview() {
    KRSUTheme() {
        Toolbar(text = "Toolbar Toolbar Toolbar Toolbar Toolbar Toolbar") {
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun Toolbar(
    text: String = "",
    onClick: () -> Unit,
) {
    Box(
        modifier = Modifier
            .height(40.dp)
            .padding(horizontal = 12.dp),
        contentAlignment = Alignment.CenterStart
    ) {
        BackArrow {
            onClick()
        }
        Text(
            text = text,
            modifier = Modifier
                .padding(horizontal = 44.dp)
                .fillMaxWidth(),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            maxLines = 1,
        )
    }
}

@ExperimentalMaterialApi
@Composable
fun BackArrow(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Card(
        onClick = { onClick() },
        modifier = modifier.size(40.dp),
        backgroundColor = MaterialTheme.colors.background,
        border = BorderStroke(
            1.dp,
            MaterialTheme.colors.onBackground.copy(alpha = 0.2f)
        ),
    ) {
        Box(
            modifier = Modifier
                .padding(end = 2.dp)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center,
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_arrow_20),
                contentDescription = stringResource(id = R.string.back),
                contentScale = ContentScale.None,
                colorFilter = ColorFilter.tint(
                    MaterialTheme.colors.onBackground.copy(
                        alpha = 0.6f
                    )
                )
            )
        }
    }
}
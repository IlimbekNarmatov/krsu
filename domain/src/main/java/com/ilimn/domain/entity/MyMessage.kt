package com.ilimn.domain.entity

private const val START = 0

sealed class MyMessage(val number: Int) : MessageType {
    object Text : MyMessage(START + 1)
}
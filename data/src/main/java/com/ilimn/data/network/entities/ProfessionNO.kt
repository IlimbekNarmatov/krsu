package com.ilimn.data.network.entities

import com.ilimn.domain.entity.Profession

internal data class ProfessionNO(
    val id: Int?,
    val name: String?,
    val icon: String?,
)

internal fun ProfessionNO.toProfession() = Profession(
    id = id ?: -1,
    name = name ?: "",
    icon = icon ?: "",
)
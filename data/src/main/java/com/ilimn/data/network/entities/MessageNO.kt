package com.ilimn.data.network.entities

import com.ilimn.domain.entity.CompanionMessage
import com.ilimn.domain.entity.Message
import com.ilimn.domain.entity.MyMessage

data class MessageNO(
    val id: Int?,
    val message: String?,
    val sentTime: String?,
    val isMine: Boolean?,
)

internal fun MessageNO.toMessage(): Message = Message(
    id = id ?: 0,
    message = message ?: "",
    sentTime = null,
    messageType = if (isMine == true) MyMessage.Text else CompanionMessage.Text
)
package com.ilimn.krsu.vm.start

import androidx.lifecycle.viewModelScope
import com.ilimn.domain.entity.OnBoarding
import com.ilimn.domain.usecase.BaseUseCase
import com.ilimn.domain.usecase.GetOnBoardingsUC
import com.ilimn.domain.usecase.OnBoardingSeenUC
import com.ilimn.domain.util.Success
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.ui.navigation.MAIN_SCREEN
import com.ilimn.krsu.ui.navigation.ON_BOARDING_SCREEN
import com.ilimn.krsu.vm.util.BaseVM
import com.ilimn.krsu.vm.util.NavigationProperty
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class OnBoardingVM(
    private val getOnBoardingsUC: GetOnBoardingsUC,
    private val onBoardingSeenUC: OnBoardingSeenUC
) : BaseVM() {
    private val _navigation =
        MutableSharedFlow<Navigation>()
    val navigation: SharedFlow<Navigation> =
        _navigation
    private val _onBoardings = MutableStateFlow<List<OnBoarding>>(
        emptyList()
    )
    val onBoardings: StateFlow<List<OnBoarding>> = _onBoardings
    private val _onBoardingSeen = MutableStateFlow<Success?>(null)
    private val mediator = combine(
        _onBoardingSeen
    ) {
        if (_onBoardingSeen.value == null) return@combine
        _navigation.emit(Navigation.GotoMain)
    }

    init {
        viewModelScope.launch {
            getOnBoardingsUC(BaseUseCase.None)
                .onSuccessSuspend { _onBoardings.emit(it) }
                .onFailureSuspend { _failure.send(it) }
        }
        viewModelScope.launch {
            mediator.collect()
        }
    }

    fun onBoardingSeen() {
        viewModelScope.launch {
            onBoardingSeenUC(BaseUseCase.None)
                .onSuccessSuspend { _onBoardingSeen.emit(it) }
                .onFailureSuspend { _failure.send(it) }
        }
    }

    sealed class Navigation : NavigationProperty {
        object GotoMain : Navigation() {
            override val route: String = MAIN_SCREEN
            override val popUpRoute = ON_BOARDING_SCREEN
            override val isInclusive: Boolean = true
        }
    }
}
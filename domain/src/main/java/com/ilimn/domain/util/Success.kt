package com.ilimn.domain.util

sealed class Success {
    object Default : Success()
}

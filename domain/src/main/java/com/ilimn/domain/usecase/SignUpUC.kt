package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Login
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SignUpUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(
        map: Map<String, String>,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, Success> = withContext(dispatcher) {
        val validations = validate(map)
        if (validations.isNotEmpty()) return@withContext Failure.Format(
            validations
        ).left()
        var result: Either<Failure, Success> = Failure.Default.left()
        repo.signUp(
            Login.Email(
                map[Login.Email.EMAIL]!!,
                map[Login.Email.PASSWORD]!!
            )
        ).onSuccessSuspend {
            result = Success.Default.right()
        }.onFailureSuspend {
            result = it.left()
        }
        result
    }

    private fun validate(map: Map<String, String>): Set<String> {
        val validEmail = ValidateEmailUC()
            .invoke(map[Login.Email.EMAIL] ?: "")
        val validPassword = ValidatePasswordUC()
            .invoke(map[Login.Email.PASSWORD] ?: "")
        return HashSet<String>().apply {
            if (!validEmail) add(Login.Email.EMAIL)
            if (!validPassword) add(Login.Email.PASSWORD)
        }
    }

}

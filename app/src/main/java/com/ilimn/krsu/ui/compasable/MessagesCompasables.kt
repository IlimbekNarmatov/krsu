package com.ilimn.krsu.ui.compasable

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.flowlayout.FlowCrossAxisAlignment
import com.google.accompanist.flowlayout.FlowRow
import com.google.accompanist.flowlayout.MainAxisAlignment
import com.ilimn.domain.entity.CompanionMessage
import com.ilimn.domain.entity.Message
import com.ilimn.domain.entity.MyMessage
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.theme.textSecondaryColor
import com.ilimn.krsu.ui.utils.time

private val myMessagePadding = PaddingValues(
    start = 16.dp,
    top = 8.dp,
    end = 8.dp,
    bottom = 8.dp
)

@Preview
@Composable
fun MyMessagesPreview() {
    KRSUTheme {
        val message = Message(
            id = 1,
            sentTime = 0,
            message = "Lorem ipsum dolor sit ameti, Lorem ipsum Lorem ipsumli Lorem ipsum lorem ipsum ilimbek narmatov ulankovich consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, ",
            messageType = MyMessage.Text
        )
        val coMessage = Message(
            id = 1,
            sentTime = 0,
            message = "Lorem ipsum dolor sit ameti, Lorem ipsum Lorem ipsumli Lorem ipsum lorem ipsum ilimbek narmatov ulankovich consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, ",
            messageType = CompanionMessage.Text
        )
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            item {
                Column(
                    modifier = Modifier
                        .padding(start = 40.dp, end = 20.dp),
                ) {
                    MyTextMessage(message = message)
                    Spacer(modifier = Modifier.height(8.dp))
                    MyLastTextMessage(message = message)
                }
            }
            item {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 20.dp, end = 40.dp),
                    horizontalAlignment = Alignment.Start
                ) {
                    Spacer(modifier = Modifier.height(12.dp))
                    CompanionTextMessage(message = coMessage)
                    Spacer(modifier = Modifier.height(8.dp))
                    CompanionLastTextMessage(message = coMessage)
                }
            }
        }
    }
}

@Composable
fun MyTextMessage(message: Message) {
    Card(
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colors.primary.copy(alpha = 0.15f)
        ),
        elevation = 0.dp,
        backgroundColor = MaterialTheme.colors.background,
    ) {
        FlowRow(
            modifier = Modifier.padding(myMessagePadding),
            mainAxisAlignment = MainAxisAlignment.End,
            crossAxisAlignment = FlowCrossAxisAlignment.End
        ) {
            Text(
                text = message.message,
            )
            Spacer(modifier = Modifier.width(16.dp))
            val time = message.time
            if (time != null) {
                Text(
                    text = time,
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.textSecondaryColor()
                )
            }
        }
    }
}

@Composable
fun MyLastTextMessage(message: Message) {
    Card(
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colors.primary.copy(alpha = 0.15f)
        ),
        elevation = 0.dp,
        shape = MaterialTheme.shapes.medium.copy(
            bottomEnd = CornerSize(
                0.dp
            )
        ),
        backgroundColor = MaterialTheme.colors.background,
    ) {
        FlowRow(
            modifier = Modifier.padding(myMessagePadding),
            mainAxisAlignment = MainAxisAlignment.End,
            crossAxisAlignment = FlowCrossAxisAlignment.End
        ) {
            Text(
                text = message.message,
            )
            Spacer(modifier = Modifier.width(16.dp))
            val time = message.time
            if (time != null) {
                Text(
                    text = time,
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.textSecondaryColor()
                )
            }
        }
    }
}

@Composable
fun CompanionTextMessage(message: Message) {
    Card(elevation = 0.dp) {
        FlowRow(
            modifier = Modifier.padding(myMessagePadding),
            mainAxisAlignment = MainAxisAlignment.End,
            crossAxisAlignment = FlowCrossAxisAlignment.End
        ) {
            Text(
                text = message.message,
            )
            Spacer(modifier = Modifier.width(16.dp))
            val time = message.time
            if (time != null) {
                Text(
                    text = time,
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.textSecondaryColor()
                )
            }
        }
    }
}

@Composable
fun CompanionLastTextMessage(message: Message) {
    Card(
        elevation = 0.dp,
        shape = MaterialTheme.shapes.medium.copy(
            bottomStart = CornerSize(
                0.dp
            )
        ),
    ) {
        FlowRow(
            modifier = Modifier.padding(myMessagePadding),
            mainAxisAlignment = MainAxisAlignment.End,
            crossAxisAlignment = FlowCrossAxisAlignment.End
        ) {
            Text(
                text = message.message,
            )
            Spacer(modifier = Modifier.width(16.dp))
            val time = message.time
            if (time != null) {
                Text(
                    text = time,
                    style = MaterialTheme.typography.caption,
                    color = MaterialTheme.colors.textSecondaryColor()
                )
            }
        }
    }
}

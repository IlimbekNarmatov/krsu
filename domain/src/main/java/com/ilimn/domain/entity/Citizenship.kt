package com.ilimn.domain.entity

data class Citizenship(val name: String) {
    companion object {
        fun empty() = Citizenship("")
    }
}

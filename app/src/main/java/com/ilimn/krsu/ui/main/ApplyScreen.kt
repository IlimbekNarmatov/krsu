@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.main

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ilimn.domain.entity.ApplyingDescription
import com.ilimn.domain.util.toAnswer
import com.ilimn.domain.util.toQuestion
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.ExpandableTextCard

@Composable
fun ApplyScreen(navController: androidx.navigation.NavController) {
    Row {
        Text(text = "Apply")
        Button(onClick = { }) {
            Text(text = "Next")
        }
    }
}

@Preview
@Composable
fun ApplyingListPreview() {
    val expandedSet = remember {
        mutableStateOf<Set<Int>>(emptySet())
    }
    val applyingDescriptions = listOf(
        ApplyingDescription(0, "Question 1", "Answer 1"),
        ApplyingDescription(1, "Question 2", "Answer 2"),
        ApplyingDescription(2, "Question 3", "Answer 3"),
        ApplyingDescription(3, "Question 4", "Answer 4"),
        ApplyingDescription(4, "Question 5", "Answer 5"),
        ApplyingDescription(5, "Question 6", "Answer 6"),
        ApplyingDescription(6, "Question 7", "Answer 7"),
        ApplyingDescription(7, "Question 8", "Answer 8"),
        ApplyingDescription(8, "Question 9", "Answer 9"),
        ApplyingDescription(9, "Question 10", "Answer 10"),
    )
    KRSUTheme {
        ApplyingList(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp),
            applyingDescriptions = applyingDescriptions,
            expanded = expandedSet,
        ) {
            expandedSet.value = HashSet<Int>().apply {
                addAll(expandedSet.value)
                if (contains(it.id)) {
                    remove(it.id)
                } else {
                    add(it.id)
                }
            }
        }
    }
}


@Composable
fun ApplyingList(
    modifier: Modifier = Modifier,
    listState: LazyListState = LazyListState(),
    applyingDescriptions: List<ApplyingDescription>,
    expanded: State<Set<Int>> = mutableStateOf(setOf()),
    onClick: (ApplyingDescription) -> Unit,
) {
    LazyColumn(
        modifier = modifier,
        state = listState,
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        items(applyingDescriptions) {
            ExpandableTextCard(
                question = it.title.toQuestion(),
                answer = it.description.toAnswer(),
                isAnswerExpanded = expanded.value.contains(it.id),
                onQuestionClick = { question -> onClick(it) },
                onAnswerClick = { answer -> onClick(it) },
            )
        }
    }
}
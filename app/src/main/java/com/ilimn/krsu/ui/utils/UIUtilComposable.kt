@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.utils

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ilimn.domain.entity.Answer
import com.ilimn.domain.entity.Question
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.theme.KRSUTheme

@ExperimentalMaterialApi
@Preview
@Composable
fun ExpandableTextCardPreview() {
    KRSUTheme {
        var isQuestionExpanded by remember {
            mutableStateOf(false)
        }
        ExpandableTextCard(
            question = Question("Faq 1"),
            answer = Answer("Answer 1"),
            isAnswerExpanded = isQuestionExpanded,
            onQuestionClick = {
                isQuestionExpanded = !isQuestionExpanded
            },
            onAnswerClick = {
                isQuestionExpanded = !isQuestionExpanded
            },
        )
    }
}

@ExperimentalMaterialApi
@Composable
fun ExpandableTextCard(
    modifier: Modifier = Modifier,
    question: Question,
    answer: Answer,
    isAnswerExpanded: Boolean = false,
    onQuestionClick: (Question) -> Unit = {},
    onAnswerClick: (Answer) -> Unit = {},
) {
    Column(modifier = modifier) {
        ArrowTextCard(question = question.question) {
            onQuestionClick(question)
        }
        AnimatedVisibility(visible = isAnswerExpanded) {
            Spacer(modifier = Modifier.height(8.dp))
            PrimaryClickAbleCard(onClick = { onAnswerClick(answer) }) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 8.dp)
                ) {
                    Text(
                        text = answer.answer
                    )
                }
            }
        }
    }
}

@ExperimentalMaterialApi
@Preview
@Composable
fun ArrowTextCardPreview() {
    KRSUTheme {
        ArrowTextCard(
            question = "Faq 1"
        ) {
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ArrowTextCard(
    question: String,
    rotation: Float = 90f,
    onClick: () -> Unit
) {
    PrimaryClickAbleCard(onClick = { onClick() }) {
        val padding = PaddingValues(
            start = 12.dp,
            top = 16.dp,
            end = 12.dp,
            bottom = 16.dp
        )
        Row(
            modifier = Modifier
                .padding(padding),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = question,
                modifier = Modifier.weight(1f)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = stringResource(id = R.string.expand),
                colorFilter = ColorFilter.tint(
                    color = MaterialTheme.colors.onSurface
                        .copy(alpha = 0.5f)
                ),
                modifier = Modifier.rotate(rotation)
            )
        }
    }
}

@ExperimentalMaterialApi
@Preview
@Composable
fun PrimaryClickAbleCardPreview() {
    KRSUTheme {
        PrimaryClickAbleCard(
            onClick = {},
        ) {
            val padding = PaddingValues(
                start = 12.dp,
                top = 16.dp,
                end = 12.dp,
                bottom = 16.dp
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(padding)
            ) {
                Text(text = "Faq 1")
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun PrimaryClickAbleCard(
    onClick: () -> Unit,
    backgroundAlpha: Float = 0.15f,
    content: @Composable () -> Unit,
) {
    Card(
        onClick = { onClick() },
        backgroundColor = MaterialTheme.colors.primarySurface.copy(
            alpha = backgroundAlpha
        ),
        elevation = 0.dp,
        indication = rememberRipple(color = MaterialTheme.colors.primary),
    ) {
        content()
    }
}

@Preview
@Composable
fun ProgressPreview() {
    KRSUTheme {
        AppProgress()
    }
}

@Composable
fun AppProgress() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Preview
@Composable
fun NotificationComposablePreview() {
    KRSUTheme {
        NotificationComposable {}
    }
}

@Composable
fun NotificationComposable(
    onClick: () -> Unit
) {
    IconButton(
        onClick = { onClick() },
        modifier = Modifier
            .clip(RoundedCornerShape(6.dp))
            .size(32.dp)
            .background(
                MaterialTheme.colors.onBackground.copy(alpha = 0.02f)
            )
    ) {
        Image(
            painter = R.drawable.ic_notification.painter,
            contentDescription = R.string.notifications.string
        )
    }
}

@Composable
fun Email(
    modifier: Modifier = Modifier,
    email: State<String> = mutableStateOf(""),
    onChange: (String) -> Unit = {},
    emailError: State<Boolean> = mutableStateOf(false),
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(),
    isErrorTextVisible: Boolean = false,
    errorContent: @Composable RowScope.() -> Unit = {},
) {
    OutlinedTextField(
        value = email.value,
        onValueChange = { onChange(it) },
        textStyle = MaterialTheme.typography.body2,
        isError = emailError.value,
        modifier = modifier,
        colors = colors,
        label = {
            Text(text = R.string.email.string)
        }
    )
    AnimatedVisibility(visible = isErrorTextVisible && emailError.value) {
        Row(modifier = Modifier.padding(top = 2.dp)) {
            Icon(
                imageVector = Icons.Filled.Info,
                contentDescription = R.string.info.string,
                tint = MaterialTheme.colors.error,
                modifier = Modifier.align(Alignment.CenterVertically),
            )
            CompositionLocalProvider(
                LocalTextStyle.provides(
                    TextStyle(
                        color = MaterialTheme.colors.error,
                        fontSize = MaterialTheme.typography.caption.fontSize,
                    )
                )
            ) {
                errorContent()
            }
        }
    }
}

@Preview
@Composable
fun PasswordPreview() {
    KRSUTheme {
        Column {
            Password(modifier = Modifier.fillMaxWidth()) {
                Text(text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor")
            }
        }
    }
}

@Composable
fun Password(
    modifier: Modifier = Modifier,
    password: State<String> = mutableStateOf(""),
    onChange: (String) -> Unit = {},
    passwordError: State<Boolean> = mutableStateOf(false),
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(),
    onSeeClick: () -> Unit = {},
    seeVisible: State<Boolean> = mutableStateOf(false),
    isErrorTextVisible: Boolean = false,
    errorContent: @Composable RowScope.() -> Unit = {},
) {
    OutlinedTextField(
        value = password.value,
        onValueChange = { onChange(it) },
        textStyle = MaterialTheme.typography.body2,
        isError = passwordError.value,
        modifier = modifier,
        colors = colors,
        label = {
            Text(text = R.string.password.string)
        },
        visualTransformation = PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password
        ),
    )
    AnimatedVisibility(visible = isErrorTextVisible && passwordError.value) {
        Row(modifier = Modifier.padding(top = 2.dp)) {
            Icon(
                imageVector = Icons.Filled.Info,
                contentDescription = R.string.info.string,
                tint = MaterialTheme.colors.error,
                modifier = Modifier.align(Alignment.CenterVertically),
            )
            CompositionLocalProvider(
                LocalTextStyle.provides(
                    TextStyle(
                        color = MaterialTheme.colors.error,
                        fontSize = MaterialTheme.typography.caption.fontSize,
                    )
                )
            ) {
                errorContent()
            }
        }
    }
}

@Preview
@Composable
fun CodeConfirmTextFieldPreview() {
    KRSUTheme {
        CodeConfirmTextField(modifier = Modifier.fillMaxWidth())
    }
}

@Composable
fun CodeConfirmTextField(
    modifier: Modifier = Modifier,
    text: State<String> = mutableStateOf(""),
    onChange: (String) -> Unit = {},
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(),
    isErrorTextVisible: Boolean = false,
    error: State<Boolean> = mutableStateOf(false),
    errorContent: @Composable RowScope.() -> Unit = {},
) {
    OutlinedTextField(
        value = text.value,
        onValueChange = { onChange(it) },
        textStyle = MaterialTheme.typography.body2,
        isError = error.value,
        modifier = modifier,
        colors = colors,
        label = {
            Text(text = R.string.code.string)
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
    )
    AnimatedVisibility(visible = isErrorTextVisible && error.value) {
        Row(
            modifier = Modifier.padding(top = 2.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                imageVector = Icons.Filled.Info,
                contentDescription = R.string.info.string,
                tint = MaterialTheme.colors.error,
                modifier = Modifier.align(Alignment.CenterVertically),
            )
            CompositionLocalProvider(
                LocalTextStyle.provides(
                    TextStyle(
                        color = MaterialTheme.colors.error,
                        fontSize = MaterialTheme.typography.caption.fontSize,
                    )
                )
            ) {
                errorContent()
            }
        }
    }
}

@Preview
@Composable
fun SendIconPreview() {
    KRSUTheme {
        SendIcon {
        }
    }
}

@Composable
fun SendIcon(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
) {
    Card(
        onClick = onClick,
        modifier = modifier.size(40.dp),
        backgroundColor = MaterialTheme.colors.primary,
        elevation = 0.dp,
        shape = MaterialTheme.shapes.medium,
    ) {
        Box(contentAlignment = Alignment.Center) {
            Icon(
                painter = R.drawable.ic_send.painter,
                contentDescription = R.string.send.string,
                tint = MaterialTheme.colors.onPrimary,
            )
        }
    }
}

@Preview
@Composable
fun MessageTextFieldPreview() {
    KRSUTheme {
        var text by remember {
            mutableStateOf("")
        }
        MessageTextField(
            modifier = Modifier.fillMaxWidth(),
            value = text,
            onValueChange = {
                text = it
            },
            label = {
                Text(text = "Label")
            },
        )
    }
}

@Composable
fun MessageTextField(
    value: String = "",
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions(),
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = MaterialTheme.shapes.medium,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(
        unfocusedBorderColor = Color.Transparent,
    )
) {
    val textColor = textStyle.color.takeOrElse {
        colors.textColor(enabled).value
    }
    val mergedTextStyle =
        textStyle.merge(TextStyle(color = textColor))
    BasicTextField(
        value = value,
        modifier = modifier
            .background(colors.backgroundColor(enabled).value, shape)
            .defaultMinSize(
                minWidth = TextFieldDefaults.MinWidth,
                minHeight = 40.dp
            ),
        onValueChange = onValueChange,
        enabled = enabled,
        readOnly = readOnly,
        textStyle = mergedTextStyle,
        cursorBrush = SolidColor(colors.cursorColor(isError).value),
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        interactionSource = interactionSource,
        singleLine = singleLine,
        maxLines = maxLines,
        decorationBox = @Composable { innerTextField ->
            TextFieldDefaults.OutlinedTextFieldDecorationBox(
                value = value,
                visualTransformation = visualTransformation,
                innerTextField = innerTextField,
                placeholder = placeholder,
                label = null,
                leadingIcon = leadingIcon,
                trailingIcon = trailingIcon,
                singleLine = singleLine,
                enabled = enabled,
                isError = isError,
                interactionSource = interactionSource,
                colors = colors,
                contentPadding = PaddingValues(12.dp),
                border = {
                    TextFieldDefaults.BorderStroke(
                        enabled,
                        isError,
                        interactionSource,
                        colors,
                        shape
                    )
                }
            )
        }
    )
}

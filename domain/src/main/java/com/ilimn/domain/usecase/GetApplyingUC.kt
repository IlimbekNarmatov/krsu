package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Applying
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class GetApplyingUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, Applying>, Int>() {

    override suspend fun run(params: Int): Either<Failure, Applying> =
        repo.getApplying(params)

}
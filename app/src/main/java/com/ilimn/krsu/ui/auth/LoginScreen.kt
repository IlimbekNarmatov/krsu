@file:OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalMaterialApi::class
)

package com.ilimn.krsu.ui.auth

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.compasable.BackArrow
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.theme.outlinedTextColors
import com.ilimn.krsu.ui.utils.*
import com.ilimn.krsu.vm.auth.LoginVM

@Composable
fun LoginScreen(
    navController: NavController,
    vm: LoginVM = viewModel(),
) {
    LaunchedEffect(Unit) {
        vm.navigation.collect { navigation ->
            if (navigation.isPopBackStack) {
                navController.popBackStack(
                    navigation.route,
                    navigation.isInclusive
                )
            } else {
                navController.navigatePopUp(navigation)
            }
        }
    }
    LoginPage(
        onEmailChange = { vm.setEmail(it) },
        onPasswordChange = { vm.setPassword(it) },
        emailError = vm.emailError,
        passwordError = vm.passwordError,
        onSignUpClick = { vm.signUp() },
        onSignInClick = { vm.signIn() },
        email = vm.email,
        password = vm.password,
        onBackClick = { navController.navigateUp() },
        scrollState = vm.pageScrollState,
    )
}

@Preview
@Composable
fun LoginPagePreview() {
    KRSUTheme {
        LoginPage()
    }
}

@Composable
fun LoginPage(
    onEmailChange: (String) -> Unit = {},
    onPasswordChange: (String) -> Unit = {},
    emailError: State<Boolean> = mutableStateOf(false),
    passwordError: State<Boolean> = mutableStateOf(false),
    onSignUpClick: () -> Unit = {},
    onSignInClick: () -> Unit = {},
    email: State<String> = mutableStateOf(""),
    password: State<String> = mutableStateOf(""),
    onBackClick: () -> Unit = {},
    scrollState: ScrollState = ScrollState(0),
) {
    Column(
        modifier = Modifier
            .fillMaxSize(),
    ) {
        BackArrow(
            modifier = Modifier.padding(
                top = 16.dp,
                start = 12.dp
            ),
            onClick = { onBackClick() },
        )
        Row(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
                .padding(top = 8.dp),
        ) {
            Spacer(modifier = Modifier.width(16.dp))
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .verticalScroll(scrollState),
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = R.string.login.string,
                    style = MaterialTheme.typography.h5,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onBackground,
                    modifier = Modifier
                        .fillMaxWidth(),
                )
                Text(
                    text = R.string.login_with_email.string,
                    style = MaterialTheme.typography.body2,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onBackground.copy(
                        0.4f
                    ),
                    modifier = Modifier
                        .fillMaxWidth(),
                )
                Spacer(modifier = Modifier.height(20.dp))
                val colors = MaterialTheme.colors.outlinedTextColors()
                Email(
                    modifier = Modifier.fillMaxWidth(),
                    email = email,
                    onChange = onEmailChange,
                    emailError = emailError,
                    colors = colors,
                )
                Password(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 2.dp),
                    password = password,
                    onChange = onPasswordChange,
                    passwordError = passwordError,
                    colors = colors,
                )
                AppGradientButton(
                    modifier = Modifier
                        .padding(top = 12.dp)
                        .fillMaxWidth(),
                    onClick = { onSignInClick() }
                ) {
                    Text(text = R.string.sign_in.string)
                }
                TextButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 2.dp),
                    onClick = { onSignUpClick() }) {
                    Text(text = R.string.sign_up.string)
                }
            }
            Spacer(modifier = Modifier.width(16.dp))
        }
    }
}
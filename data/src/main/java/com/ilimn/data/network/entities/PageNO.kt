package com.ilimn.data.network.entities

data class PageNO<T>(
    val count: Int?,
    val next: Int?,
    val previous: Int?,
    val results: List<T>
)

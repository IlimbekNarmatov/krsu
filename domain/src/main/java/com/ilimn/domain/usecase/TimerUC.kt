package com.ilimn.domain.usecase

import com.ilimn.domain.entity.TimerState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class TimerUC {

    private var restTime: Long? = null

    suspend operator fun invoke(
        params: Params = Params()
    ): Flow<TimerState> = flow {
        emit(TimerState.Started)
        restTime = params.duration
        while (restTime != null && (restTime ?: 0) > 0) {
            delay(params.tickRange)
            emit(TimerState.Tick(restTime ?: 0))
            restTime = restTime?.minus(params.tickRange)
        }
        emit(TimerState.Finished)
    }

    data class Params(
        val duration: Long = 60_000L,
        val tickRange: Long = 1_000L
    )

}

package com.ilimn.krsu.vm.main

import android.util.Log
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.ilimn.domain.entity.Profession
import com.ilimn.domain.usecase.GetProfessionsUC
import com.ilimn.domain.util.onSuccess
import com.ilimn.domain.util.tryCast
import com.ilimn.krsu.ui.navigation.PROFESSION_SCREEN_NO_ARG
import com.ilimn.krsu.vm.util.BaseVM
import com.ilimn.krsu.vm.util.NavigationProperty
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class MainVM(
    private val getProfessionsUC: GetProfessionsUC,
) : BaseVM() {

    val listState = LazyListState()
    private val _navigation =
        MutableSharedFlow<Navigation>()
    val navigation: SharedFlow<Navigation> =
        _navigation
    private val _searchText = mutableStateOf("")
    val searchText: State<String> = _searchText
    private var professions2: Flow<PagingData<Profession>>? = null

    init {
        Log.println(Log.ASSERT, "MainVM", "init: ") // LOG delete
    }

    fun goToProfession(id: Int) {
        viewModelScope.launch {
            _navigation.emit(Navigation.GotoProfession(id))
        }
    }

    fun setSearchText(text: String) {
        _searchText.value = text
    }

    fun openSite() {
        viewModelScope.launch {
            _navigation.emit(Navigation.GotoSite)
        }
    }

    fun getProfessionsPagination(): Flow<PagingData<Profession>>? {
        if (professions2 != null) {
            return professions2
        }
        var paging: Flow<PagingData<Profession>>? = null
        getProfessionsUC().onSuccess { source ->
            tryCast<PagingSource<Int, Profession>>(source) {
                paging = Pager(
                    PagingConfig(
                        pageSize = 20,
                    ),
                    pagingSourceFactory = {
                        it
                    }
                ).flow
            }
        }
        professions2 = paging
        return paging
    }

    sealed class Navigation : NavigationProperty {
        data class GotoProfession(val id: Int) : Navigation() {
            override val route: String = PROFESSION_SCREEN_NO_ARG + id
        }

        object GotoSite : Navigation() {
        }
    }

    val professions = listOf(
        Profession(id = 1, name = "profession 1", icon = null),
        Profession(id = 2, name = "profession 2", icon = null),
        Profession(id = 3, name = "profession 3", icon = null),
        Profession(id = 4, name = "profession 4", icon = null),
        Profession(id = 5, name = "profession 5", icon = null),
        Profession(id = 6, name = "profession 6", icon = null),
        Profession(id = 7, name = "profession 7", icon = null),
        Profession(id = 8, name = "profession 8", icon = null),
        Profession(id = 9, name = "profession 9", icon = null),
        Profession(id = 10, name = "profession 10", icon = null),
        Profession(id = 11, name = "profession 11", icon = null),
        Profession(id = 12, name = "profession 12", icon = null),
        Profession(id = 13, name = "profession 13", icon = null),
        Profession(id = 14, name = "profession 14", icon = null),
        Profession(id = 15, name = "profession 15", icon = null),
        Profession(id = 16, name = "profession 16", icon = null),
        Profession(id = 17, name = "profession 17", icon = null),
        Profession(id = 18, name = "profession 18", icon = null),
        Profession(id = 19, name = "profession 19", icon = null),
        Profession(id = 20, name = "profession 20", icon = null),
        Profession(id = 21, name = "profession 21", icon = null),
    )
}
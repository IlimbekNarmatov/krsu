package com.ilimn.domain.util

sealed class NewFailure {
    sealed class Network : NewFailure() {
        data class BadRequest(val message: String?) : Network()
        object Unauthorized : Network()
        object Forbidden : Network()
        object NotFound : Network()
        object BigEntity : Network()
        object Server : Network()
        object NoConnection : Network()
        data class Default(val code: Int) : Network()
    }

    sealed class DB : NewFailure() {
    }

    sealed class Convert : NewFailure() {
        object WrongType : NewFailure()
        object NotEnoughInformation : NewFailure()
    }

    object Parse : NewFailure()

    abstract class Custom : NewFailure()
}
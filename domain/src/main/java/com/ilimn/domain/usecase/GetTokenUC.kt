package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Token
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class GetTokenUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, Token>, BaseUseCase.None>() {

    override suspend fun run(
        params: None
    ): Either<Failure, Token> = repo.getToken()

}

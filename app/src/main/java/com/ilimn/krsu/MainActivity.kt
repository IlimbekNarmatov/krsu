package com.ilimn.krsu

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.ilimn.krsu.ui.navigation.SPLASH_SCREEN
import com.ilimn.krsu.ui.navigation.graph
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.BottomNavigationBar
import com.ilimn.krsu.ui.utils.bottomBarItems

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            KRSUApp()
        }
    }
}

@Preview
@Composable
fun AppPreview() {
    KRSUApp()
}

@Composable
fun KRSUApp() {
    KRSUTheme {
        rememberSystemUiController().setStatusBarColor(
            MaterialTheme.colors.primaryVariant
        )

        val navController = rememberNavController()

        var isBottomVisible by remember {
            mutableStateOf(false)
        }

        navController.addOnDestinationChangedListener() { _, destination, _ ->
            isBottomVisible = bottomBarItems.map {
                it.route
            }.toMutableList().also {
            }.contains(destination.route)
        }

        Surface {
            Scaffold(
                bottomBar = {
                    AnimatedVisibility(
                        visible = isBottomVisible,
                        enter = fadeIn(),
                        exit = fadeOut(),
                    ) {
                        BottomNavigationBar(navController = navController)
                    }
                },
            ) {
                NavHost(
                    navController = navController,
                    startDestination = SPLASH_SCREEN
                ) {
                    graph(navController)
                }
            }
        }
    }
}

@file:OptIn(ExperimentalFoundationApi::class)

package com.ilimn.krsu.ui.theme

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.gestures.OverScrollConfiguration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = Purple200,
    primaryVariant = Purple700,
    secondary = Teal200
)

private val LightColorPalette = lightColors(
    primary = BluePrimary,
    primaryVariant = BluePrimaryVariant,
    secondary = BluePrimary,
    secondaryVariant = BluePrimary,
    surface = LightBlue,
    onSurface = Color.Black,
    /* Other default colors to override
    background = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    */
)

@Composable
fun KRSUTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = LightColorPalette
//    if (darkTheme) {
//        DarkColorPalette
//    } else {
//        LightColorPalette
//    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
    ) {
        CompositionLocalProvider(
            LocalOverScrollConfiguration.provides(
                OverScrollConfiguration(
                    MaterialTheme.colors.primary.copy(alpha = 0.5f)
                )
            )
        ) {
            content()
        }
    }
}


@Composable
fun Colors.textPrimaryColor(
    isDarkTheme: Boolean = false
): Color = Color(0xFF230B34)

@Composable
fun Colors.textSecondaryColor(
    isDarkTheme: Boolean = false
): Color = Color(0xFF616B91)

@Composable
fun Colors.outlinedTextColors() =
    TextFieldDefaults.outlinedTextFieldColors(
        textColor = MaterialTheme.colors.textPrimaryColor(),
        unfocusedBorderColor = MaterialTheme.colors.primary.copy(
            0.3f
        ),
        unfocusedLabelColor = MaterialTheme.colors.primary.copy(
            0.5f
        ),
    )


package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetCountriesUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke() = withContext(Dispatchers.Default) {
        repo.getCountries()
    }

}
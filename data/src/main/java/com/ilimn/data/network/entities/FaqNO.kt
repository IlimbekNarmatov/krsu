package com.ilimn.data.network.entities

import com.ilimn.domain.entity.Faq

internal data class FaqNO(
    val id: Int?,
    val question: String?,
    val answer: String?,
)

internal fun FaqNO.toFaq() = Faq(
    id = id ?: -1,
    question = question ?: "",
    answer = answer ?: "",
)

package com.ilimn.domain.usecase

import com.ilimn.domain.entity.OnBoarding
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class GetOnBoardingsUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, List<OnBoarding>>, BaseUseCase.None>() {

    override suspend fun run(params: None): Either<Failure, List<OnBoarding>> =
        repo.getOnBoardings()

}

package com.ilimn.krsu.ui.utils

import android.icu.text.SimpleDateFormat
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavOptionsBuilder
import com.ilimn.domain.entity.Message
import com.ilimn.krsu.vm.util.NavigationProperty
import java.util.*

val Message.time: String?
    get() = sentTime?.let {
        val date = Date(it)
        SimpleDateFormat("HH:mm", Locale.getDefault())
            .format(date)
    }

val Int.painter: Painter
    @Composable
    get() = painterResource(id = this)

val Int.string: String
    @Composable
    get() = stringResource(id = this)

fun NavController.navigatePopUp(
    navigation: NavigationProperty
) {
    navigate(navigation.route) {
        navigatePopUp(
            navigation.popUpRoute,
            navigation.isInclusive
        )
    }
}

fun NavOptionsBuilder.navigatePopUp(
    route: String?,
    isInclusively: Boolean
) {
    if (route != null) {
        popUpTo(route) {
            inclusive = isInclusively
        }
    }
}
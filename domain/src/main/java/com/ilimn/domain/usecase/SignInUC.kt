package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Login
import com.ilimn.domain.entity.Login.Email.Companion.EMAIL
import com.ilimn.domain.entity.Login.Email.Companion.PASSWORD
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SignInUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(
        map: Map<String, String>,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, Success> = withContext(dispatcher) {
        val validations = validate(map)
        if (validations.isNotEmpty()) return@withContext Failure.Format(
            validations
        ).left()
        var result: Either<Failure, Success> = Failure.Default.left()
        repo.signIn(Login.Email(map[EMAIL]!!, map[PASSWORD]!!))
            .onSuccessSuspend {
                SetTokenUC(repo).invoke(it)
                result = Success.Default.right()
            }
            .onFailureSuspend {
                result = it.left()
            }
        result
    }

    private fun validate(map: Map<String, String>): Set<String> {
        val validEmail = ValidateEmailUC()
            .invoke(map[EMAIL] ?: "")
        return HashSet<String>().apply {
            if (!validEmail) add(EMAIL)
        }
    }

}
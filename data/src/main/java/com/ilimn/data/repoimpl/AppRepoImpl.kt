package com.ilimn.data.repoimpl

import androidx.paging.PagingSource
import com.ilimn.data.network.entities.VersionNO
import com.ilimn.data.network.entities.toVersion
import com.ilimn.domain.entity.*
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.Success
import com.ilimn.domain.util.right
import kotlinx.coroutines.delay

internal class AppRepoImpl(
    private val dumpDataSource: DataSource,
) : AppRepo {

    private var nextKey: Int? = 1

    override suspend fun getToken(): Either<Failure, Token> {
        return Token("").right()
    }

    override suspend fun setToken(token: Token): Either<Failure, Success> {
        return Success.Default.right()
    }

    override suspend fun getVersion(): Either<Failure, Version> {
        return VersionNO(name = "1.0", isForce = false)
            .toVersion()
            .right()
    }

    override suspend fun isOnBoardingSeen(): Either<Failure, Boolean> {
        return true.right()
    }

    override suspend fun onBoardingSeen(): Either<Failure, Success> {
        return Success.Default.right()
    }

    override suspend fun isLoggedIn(): Either<Failure, Boolean> {
        return true.right()
    }

    override fun getProfessions(): Either<Failure, PagingSource<Int, Profession>> {
        return dumpDataSource.getProfessionsPaging().right()
    }

    override suspend fun searchProfession(keyword: String): Either<Failure, List<Profession>> {
        return listOf<Profession>()
            .right()
    }

    override suspend fun getDetailProfession(id: Int): Either<Failure, DetailProfession> {
        delay(1_000)
        return dumpDataSource.getDetailProfession(id)
    }

    override suspend fun getApplying(id: Int): Either<Failure, Applying> {
        return Applying(
            descriptions = listOf(),
            images = listOf(),
        ).right()
    }

    override fun getFaqs(): Either<Failure, PagingSource<Int, Faq>> {
        return dumpDataSource.getFaqs()
    }

    override suspend fun signIn(login: Login): Either<Failure, Token> {
        return Token("").right()
    }

    override suspend fun signUp(login: Login): Either<Failure, Success> {
        return Success.Default.right()
    }

    override suspend fun confirm(confirm: Confirm): Either<Failure, Token> {
        return Token("").right()
    }

    override suspend fun sendMessage(message: Message): Either<Failure, Success> {
        return dumpDataSource.sendMessage(message)
    }

    override fun getMessagesPaging(): PagingSource<Int, Message> {
        return dumpDataSource.getMessagesPaging()
    }

    override suspend fun getProfile(): Either<Failure, Profile> {
        return Profile(
            firstName = "Toktosun",
            lastName = "Suksurali uulu",
            citizenship = Citizenship("Kyrgyzstan"),
            school = School("B.Osmonov"),
        ).right()
    }

    override suspend fun getOnBoardings(): Either<Failure, List<OnBoarding>> =
        listOf(
            OnBoarding(
                0,
                title = "Lorem ipsum\nlike this",
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
                images = listOf("https://picsum.photos/id/1/200/200")
            ),
            OnBoarding(
                1,
                title = "Lorem ipsum\nlike this 2",
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
                images = listOf("https://picsum.photos/id/2/200/200")
            ),
        ).right()

    override suspend fun updateProfile(
        profile: Profile
    ): Either<Failure, Profile> = Profile(
        firstName = "Ilimbek",
        lastName = "Narmatov",
        citizenship = Citizenship("Kyrgyzstan"),
        school = School("B.Osmonov"),
    ).right()

    override suspend fun getCountries(): Either<Failure, List<Citizenship>> =
        dumpDataSource.getCountries()

    override suspend fun getMessages(
        page: Int,
        size: Int
    ): Either<Failure, List<Message>> =
        dumpDataSource.getMessages(page, size)
}
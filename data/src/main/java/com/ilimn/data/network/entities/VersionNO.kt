package com.ilimn.data.network.entities

import com.ilimn.domain.entity.Version

internal data class VersionNO(
    val name: String,
    val isForce: Boolean
)

internal fun VersionNO.toVersion() = Version(
    name = name,
    isForceRefresh = isForce
)

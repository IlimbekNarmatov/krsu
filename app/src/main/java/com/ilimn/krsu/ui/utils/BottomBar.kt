package com.ilimn.krsu.ui.utils

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.navigation.FAQ_SCREEN
import com.ilimn.krsu.ui.navigation.MAIN_SCREEN
import com.ilimn.krsu.ui.navigation.PROFILE_SCREEN

val bottomBarHeight = 56.dp

data class BottomNavItem(
    @StringRes
    val label: Int,
    @DrawableRes
    val icon: Int,
    val route: String
)

val bottomBarItems = listOf(
    BottomNavItem(
        label = R.string.home,
        icon = R.drawable.ic_home,
        route = MAIN_SCREEN
    ),
    BottomNavItem(
        label = R.string.faq,
        icon = R.drawable.ic_message,
        route = FAQ_SCREEN
    ),
    BottomNavItem(
        label = R.string.profile,
        icon = R.drawable.ic_profile,
        route = PROFILE_SCREEN
    ),
)

@Composable
fun BottomNavigationBar(navController: NavController) {
    BottomNavigation(
        backgroundColor = MaterialTheme.colors.surface,
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route
        bottomBarItems.forEach { navItem ->
            BottomNavigationItem(
                selected = currentRoute == navItem.route,
                onClick = {
                    navController.navigate(navItem.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        restoreState = true
                        launchSingleTop = true
                    }
                },
                icon = {
                    Icon(
                        painter = navItem.icon.painter,
                        contentDescription = navItem.label.string,
                    )
                },
                label = {
                    Text(
                        text = navItem.label.string,
                        style = MaterialTheme.typography.overline,
                    )
                },
                alwaysShowLabel = true,
                selectedContentColor = MaterialTheme.colors.primary,
                unselectedContentColor = MaterialTheme.colors.primary
                    .copy(
                        alpha = 0.6f
                    )
            )
        }
    }
}

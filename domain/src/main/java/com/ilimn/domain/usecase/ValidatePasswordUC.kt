package com.ilimn.domain.usecase

class ValidatePasswordUC {

    operator fun invoke(password: String): Boolean {
        return password.length >= 8
    }
}
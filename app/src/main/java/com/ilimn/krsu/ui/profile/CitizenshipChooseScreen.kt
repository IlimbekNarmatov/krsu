@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.profile

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ilimn.domain.entity.Citizenship
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.PrimaryClickAbleCard
import kotlinx.coroutines.launch

@Preview
@Composable
fun CitizenshipBottomPagePreview() {
    KRSUTheme {
        val modalSheetState: ModalBottomSheetState =
            rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
        val coroutineScope = rememberCoroutineScope()
        ModalBottomSheetLayout(
            sheetContent = {
                CitizenshipBottomPage(
                    onSelected = {
                        coroutineScope.launch { modalSheetState.hide() }
                    },
                    countries = remember {
                        mutableStateOf(
                            listOf(
                                Citizenship("Kyrgyzstan 1"),
                                Citizenship("Kyrgyzstan 2"),
                                Citizenship("Kyrgyzstan 3"),
                                Citizenship("Kyrgyzstan 4"),
                            )
                        )
                    }
                )
            },
            sheetState = modalSheetState,
        ) {
            Button(onClick = {
                coroutineScope.launch {
                    modalSheetState.show()
                }
            }) {
                Text(text = "Show")
            }
        }
    }
}

@Composable
fun CitizenshipBottomPage(
    onSelected: (Citizenship) -> Unit = {},
    countries: State<List<Citizenship>> = mutableStateOf(emptyList()),
) {
    LazyColumn(
        contentPadding = PaddingValues(top = 8.dp),
    ) {
        itemsIndexed(countries.value) { i, item ->
            val padding = PaddingValues(
                start = 8.dp,
                end = 8.dp,
                top = 8.dp,
                bottom = if (i == countries.value.size - 1) 8.dp else 0.dp
            )
            Box(modifier = Modifier.padding(padding)) {
                Citizenship(citizenship = item) {
                    onSelected(it)
                }
            }
        }
    }
}

@Preview
@Composable
fun CitizenshipPreview() {
    KRSUTheme {
        Citizenship(
            citizenship = Citizenship(
                "Kyrgyzstan"
            )
        )
    }
}

@Composable
fun Citizenship(
    citizenship: Citizenship,
    onClick: (Citizenship) -> Unit = {},
) {
    PrimaryClickAbleCard(
        onClick = { onClick(citizenship) },
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    vertical = 16.dp,
                    horizontal = 8.dp
                ),
        ) {
            Text(text = citizenship.name)
        }
    }
}
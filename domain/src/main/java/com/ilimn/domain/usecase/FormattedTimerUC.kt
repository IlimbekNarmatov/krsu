package com.ilimn.domain.usecase

import com.ilimn.domain.entity.TimerState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class FormattedTimerUC {

    suspend operator fun invoke(
        params: TimerUC.Params = TimerUC.Params(),
        format: String = "%d:%02d",
    ): Flow<TimerState> {
        return TimerUC().invoke(params)
            .map {
                if (it !is TimerState.Tick) {
                    it
                } else {
                    val time = formatTime(it.time, format)
                    TimerState.TickString(time)
                }
            }
    }

    private fun formatTime(time: Long, format: String): String {
        val timeInSec = time / 1_000
        val hours = timeInSec / 3600
        val minutes = (timeInSec % 3600) / 60
        val seconds = timeInSec % 60
        return String.format(
            format,
            minutes,
            seconds
        )
    }
}
package com.ilimn.domain.entity

sealed interface TimerState {
    object Started : TimerState
    object Finished : TimerState
    data class Tick(val time: Long) : TimerState
    data class TickString(val time: String) : TimerState
}
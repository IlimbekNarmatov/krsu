package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Profile
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UpdateProfileUC(
    private val repo: AppRepo,
    private val isLoggedInUC: IsLoggedInUC,
) {

    suspend operator fun invoke(
        profile: Profile,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, Profile> = withContext(dispatcher) {
        var result: Either<Failure, Profile> = Failure.Default.left()
        val validations = validate(profile)
        if (validations.isNotEmpty()) return@withContext Failure.Format(
            validations
        ).left()
        isLoggedInUC()
            .onSuccessSuspend {
                result = if (it) {
                    repo.updateProfile(profile)
                } else {
                    Failure.Network.Unauthorized.left()
                }
            }
            .onFailureSuspend {
                result = it.left()
            }
        result
    }

    private fun validate(profile: Profile): Set<String> {
        val result = HashSet<String>()
        profile.firstName.ifBlank { result.add(Profile.FIRST_NAME) }
        profile.lastName.ifBlank { result.add(Profile.LAST_NAME) }
        profile.citizenship.name.ifBlank {
            result.add(Profile.CITIZENSHIP)
        }
        profile.school.name.ifBlank {
            result.add(Profile.SCHOOL)
        }
        return result
    }

}
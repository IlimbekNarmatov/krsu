package com.ilimn.krsu.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val DarkBlue = Color(0xFF1e88e5)
val DarkBlueLight = Color(0xFF6ab7ff)
val Blue = Color(0xFF1e88e5)
val BlueLight = Color(0xFF6ab7ff)
val LightBlue = Color(0xFFF2F6FF)
val BluePrimary = Color(0xFF4A57F3)
val BluePrimaryVariant = Color(0xFF4D77ED)

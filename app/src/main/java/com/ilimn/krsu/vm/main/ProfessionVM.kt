package com.ilimn.krsu.vm.main

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.lazy.LazyListState
import androidx.lifecycle.viewModelScope
import com.ilimn.domain.entity.DetailProfession
import com.ilimn.domain.usecase.GetDetailProfessionUC
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.vm.util.BaseVM
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class ProfessionVM(
    private val getDetailProfessionUC: GetDetailProfessionUC,
) : BaseVM() {
    val imagesState = LazyListState()
    val applyingState = LazyListState()
    val parentScroll = ScrollState(0)
    private val _detailProfession =
        MutableStateFlow<DetailProfession?>(null)
    val detailProfession: StateFlow<DetailProfession?> =
        _detailProfession
    private var professionId: Int? = null

    fun getDetailProfession(id: Int) {
        professionId = id
        viewModelScope.launch {
            getDetailProfessionUC(id)
                .onSuccessSuspend {
                    _detailProfession.emit(it)
                }
                .onFailureSuspend { _failure.send(it) }
        }
    }

}
package com.ilimn.krsu

import android.app.Application
import com.ilimn.data.di.apiServiceModule
import com.ilimn.data.di.dataUtilsModule
import com.ilimn.data.di.repoModule
import com.ilimn.krsu.di.useCaseModule
import com.ilimn.krsu.di.utilsModule
import com.ilimn.krsu.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@App)
            modules(
                viewModelModule,
                useCaseModule,
                repoModule,
                utilsModule,
                apiServiceModule,
                dataUtilsModule
            )
        }
    }

}
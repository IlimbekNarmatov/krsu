package com.ilimn.data.repoimpl

import androidx.paging.PagingSource
import com.ilimn.domain.entity.*
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.Success

internal interface DataSource {
    fun getFaqs(): Either<Failure, PagingSource<Int, Faq>>
    fun getCountries(): Either<Failure, List<Citizenship>>
    fun getMessagesPaging(): PagingSource<Int, Message>
    fun getMessages(
        page: Int,
        size: Int
    ): Either<Failure, List<Message>>

    fun sendMessage(
        message: Message
    ): Either<Failure, Success>

    fun getProfessionsPaging(): PagingSource<Int, Profession>
    suspend fun getDetailProfession(id: Int): Either<Failure, DetailProfession>
}
package com.ilimn.krsu.ui.start

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navigation
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.navigation.ON_BOARDING_SCREEN
import com.ilimn.krsu.ui.navigation.SPLASH_SCREEN
import com.ilimn.krsu.ui.navigation.START_GRAPH
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.navigatePopUp
import com.ilimn.krsu.ui.utils.painter
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.start.SplashVM
import org.koin.androidx.compose.getViewModel


@Preview
@Composable
fun SplashScreenPreview() {
    KRSUTheme {
        SplashScreen(navController = rememberNavController())
    }
}

@Preview
@Composable
fun SplashScreenDeployPreview() {
    KRSUTheme {
        val navController = rememberNavController()
        NavHost(
            navController = navController,
            startDestination = START_GRAPH
        ) {
            navigation(
                startDestination = SPLASH_SCREEN,
                route = START_GRAPH
            ) {
                composable(route = SPLASH_SCREEN) {
                    val vm = getViewModel<SplashVM>()
                    SplashScreen(
                        navController,
                        vm
                    )
                }
                composable(route = ON_BOARDING_SCREEN) {
                    OnBoardingScreen(
                        navController
                    )
                }
            }
        }
    }
}

@Composable
fun SplashScreen(
    navController: NavController,
    vm: SplashVM = viewModel()
) {
    LaunchedEffect(Unit) {
        vm.navigation
            .collect { navigation ->
                navController.navigatePopUp(navigation)
            }
    }
    val gradientColors = listOf(
        MaterialTheme.colors.primaryVariant,
        MaterialTheme.colors.primary,
    )
    BoxWithConstraints {
        maxHeight
        val screenDensity =
            LocalContext.current.resources.displayMetrics.densityDpi / 160f
        val height = maxHeight.value * screenDensity
        val width = maxWidth.value * screenDensity
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.radialGradient(
                        gradientColors,
                        center = Offset(width, height / 4),
                        radius = width,
                    )
                )
        ) {
            val image = createRef()
            Image(
                painter = R.drawable.img_logo.painter,
                contentDescription = R.string.logo.string,
                modifier = Modifier
                    .width(132.dp)
                    .height(146.dp)
                    .constrainAs(image) {
                        centerHorizontallyTo(parent)
                        centerVerticallyTo(parent, bias = 0.25f)
                    }
            )
        }
    }
}

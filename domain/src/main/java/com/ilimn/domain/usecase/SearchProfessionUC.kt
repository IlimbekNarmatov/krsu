package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Profession
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class SearchProfessionUC(
    private val repo: AppRepo
) : BaseUseCase<Either<Failure, List<Profession>>, String>() {

    override suspend fun run(params: String): Either<Failure, List<Profession>> =
        repo.searchProfession(params)

}
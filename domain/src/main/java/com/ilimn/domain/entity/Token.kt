package com.ilimn.domain.entity

data class Token(
    val token: String
)

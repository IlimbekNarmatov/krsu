package com.ilimn.domain.entity

data class Link(
    val id: Int? = null,
    val link: String,
    val title: String? = null,
    val icon: String? = null
)

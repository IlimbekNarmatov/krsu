package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Profile
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetProfileUC(
    private val repo: AppRepo,
    private val isLoggedInUC: IsLoggedInUC,
) {

    suspend operator fun invoke(
        dispatcher: CoroutineDispatcher = Dispatchers.Default
    ): Either<Failure, Profile> = withContext(dispatcher) {
        var result: Either<Failure, Profile> = Failure.Default.left()
        isLoggedInUC()
            .onSuccessSuspend {
                result = if (it) {
                    repo.getProfile()
                } else {
                    Failure.Network.Unauthorized.left()
                }
            }
            .onFailureSuspend {
                result = it.left()
            }
        result
    }

}
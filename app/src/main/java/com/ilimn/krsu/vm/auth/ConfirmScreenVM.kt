package com.ilimn.krsu.vm.auth

import android.os.Bundle
import androidx.compose.foundation.ScrollState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.viewModelScope
import com.ilimn.domain.entity.Confirm
import com.ilimn.domain.entity.TimerState
import com.ilimn.domain.usecase.ConfirmUC
import com.ilimn.domain.usecase.FormattedTimerUC
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.ui.navigation.ARG_EMAIL
import com.ilimn.krsu.ui.navigation.LOGIN_SCREEN
import com.ilimn.krsu.vm.util.BaseVM
import com.ilimn.krsu.vm.util.NavigationProperty
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class ConfirmScreenVM(
    private val confirmUC: ConfirmUC,
    private val formattedTimerUC: FormattedTimerUC,
    private val arguments: Bundle,
) : BaseVM() {

    private val _navigation = MutableSharedFlow<Navigation>()
    val navigation: SharedFlow<Navigation> = _navigation
    private var email: String? = arguments.getString(ARG_EMAIL)
    private val _timer = MutableStateFlow<TimerState?>(null)
    val timer: StateFlow<TimerState?> = _timer
    private var timerJob: Job? = null
    val scrollState = ScrollState(0)
    private val _confirmationCode = mutableStateOf("")
    val confirmationCode: State<String> = _confirmationCode
    private val _confirmationCodeIsWrong = mutableStateOf(false)
    val confirmationCodeIsWrong: State<Boolean> =
        _confirmationCodeIsWrong

    init {
        timerJob = launchTimer()
    }

    private fun launchTimer(): Job = viewModelScope.launch {
        val timerState: Flow<TimerState> = formattedTimerUC()
        timerState.collect {
            _timer.emit(it)
        }
    }

    fun setConfirmationCode(code: String) {
        if (code.isNotEmpty() && !code.isDigitsOnly()) return
        _confirmationCode.value = code
        _confirmationCodeIsWrong.value = false
    }

    fun resend() {
        if (timerJob?.isCompleted != true) return
        timerJob = launchTimer()
    }

    fun confirm() {
        viewModelScope.launch {
            confirmUC(
                Confirm.Email(
                    email ?: "",
                    confirmationCode.value
                )
            ).onSuccessSuspend {
                _navigation.emit(Navigation.GotoBackAfterSign)
            }.onFailureSuspend { _failure.send(it) }
        }
    }

    sealed class Navigation : NavigationProperty {
        object GotoBackAfterSign : Navigation() {
            override val route: String = LOGIN_SCREEN
            override val isInclusive: Boolean = true
            override val isPopBackStack: Boolean = true
        }
    }
}
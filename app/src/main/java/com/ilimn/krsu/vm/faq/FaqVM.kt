package com.ilimn.krsu.vm.faq

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.ilimn.domain.entity.Faq
import com.ilimn.domain.usecase.GetFaqsUC
import com.ilimn.domain.usecase.IsLoggedInUC
import com.ilimn.domain.util.*
import com.ilimn.krsu.ui.navigation.ASQ_QUESTION_SCREEN
import com.ilimn.krsu.ui.navigation.LOGIN_SCREEN
import com.ilimn.krsu.vm.util.BaseVM
import com.ilimn.krsu.vm.util.NavigationProperty
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class FaqVM(
    private val isLoggedInUC: IsLoggedInUC,
    private val getFaqsUC: GetFaqsUC,
) : BaseVM() {

    val questionsState = LazyListState()
    private val _expandedQuestions =
        mutableStateOf<Set<Int>>(emptySet())
    val expandedQuestions: State<Set<Int>> = _expandedQuestions
    private val _navigation = MutableSharedFlow<Navigation>()
    val navigation: SharedFlow<Navigation> = _navigation
    private var faqs: Flow<PagingData<Faq>>? = null

    fun toggleFaq(faq: Faq) {
        _expandedQuestions.value = HashSet<Int>().apply {
            addAll(_expandedQuestions.value)
            toggle(faq.id)
        }
    }

    fun getFaqPagination(): Flow<PagingData<Faq>>? {
        if (faqs != null) {
            return faqs
        }
        var paging: Flow<PagingData<Faq>>? = null
        getFaqsUC().onSuccess { source ->
            tryCast<PagingSource<Int, Faq>>(source) {
                paging = Pager(
                    PagingConfig(
                        pageSize = 20,
                    ),
                    pagingSourceFactory = {
                        it
                    }
                ).flow
            }
        }
        faqs = paging
        return paging
    }

    fun gotoMessageUsClick() {
        viewModelScope.launch {
            isLoggedInUC()
                .onSuccessSuspend {
                    val navigation = if (it) {
                        Navigation.GotoWriteUs
                    } else Navigation.GotoLogin
                    _navigation.emit(navigation)
                }
                .onFailureSuspend { _failure.send(it) }
        }
    }

    fun gotoNotifications() {
        viewModelScope.launch {
            isLoggedInUC()
                .onSuccessSuspend {
                    val navigation = if (it) {
                        Navigation.GotoNotifications
                    } else Navigation.GotoLogin
                    _navigation.emit(navigation)
                }
                .onFailureSuspend { _failure.send(it) }
        }
    }

    sealed class Navigation : NavigationProperty {
        object GotoNotifications : Navigation()

        object GotoWriteUs : Navigation() {
            override val route: String = ASQ_QUESTION_SCREEN
        }

        object GotoLogin : Navigation() {
            override val route: String = LOGIN_SCREEN
        }
    }

}
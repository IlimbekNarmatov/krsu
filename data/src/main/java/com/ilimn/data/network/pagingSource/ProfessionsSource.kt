package com.ilimn.data.network.pagingSource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ilimn.data.network.entities.PageNO
import com.ilimn.data.network.entities.ProfessionNO
import com.ilimn.data.network.entities.toProfession
import com.ilimn.data.util.getException
import com.ilimn.domain.entity.Profession
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

internal class ProfessionsSource(
    private val fn: suspend (page: Int) -> Either<Failure, PageNO<ProfessionNO>>
) : PagingSource<Int, Profession>() {

    override fun getRefreshKey(
        state: PagingState<Int, Profession>
    ): Int? {
        return state.anchorPosition
    }

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Profession> =
        when (val response = fn(params.key ?: 1)) {
            is Either.Left -> {
                LoadResult.Error(response.l.getException())
            }
            is Either.Right -> {
                LoadResult.Page(
                    data = response.r.results.map {
                        it.toProfession()
                    },
                    prevKey = null,
                    nextKey = response.r.next
                )
            }
        }
}
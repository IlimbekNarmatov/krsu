package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Confirm
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ConfirmUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(
        confirm: Confirm,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, Success> = withContext(dispatcher) {
        var result: Either<Failure, Success> = Failure.Default.left()
        repo.confirm(confirm)
            .onSuccessSuspend {
                SetTokenUC(repo).invoke(it)
                result = Success.Default.right()
            }
            .onFailureSuspend {
                result = it.left()
            }
        result
    }

}
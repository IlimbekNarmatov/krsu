package com.ilimn.domain.entity

private const val START = 1000

sealed class CompanionMessage(val number: Int) : MessageType {
    object Text : CompanionMessage(START + 1)
}

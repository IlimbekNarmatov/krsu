@file:OptIn(ExperimentalMaterialApi::class)

package com.ilimn.krsu.ui.auth

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.ilimn.domain.entity.TimerState
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.compasable.BackArrow
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.theme.outlinedTextColors
import com.ilimn.krsu.ui.utils.AppGradientButton
import com.ilimn.krsu.ui.utils.CodeConfirmTextField
import com.ilimn.krsu.ui.utils.navigatePopUp
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.auth.ConfirmScreenVM

@Composable
fun ConfirmScreen(
    navController: NavController,
    email: String,
    vm: ConfirmScreenVM = viewModel(),
) {
    LaunchedEffect(Unit) {
        vm.navigation.collect { navigation ->
            if (navigation.isPopBackStack) {
                navController.popBackStack(
                    navigation.route,
                    navigation.isInclusive
                )
            } else {
                navController.navigatePopUp(navigation)
            }
        }
    }
    ConfirmationPage(
        email = email,
        onWrongEmailClick = { navController.navigateUp() },
        confirmationCode = vm.confirmationCode,
        onConfirmationChange = { vm.setConfirmationCode(it) },
        onConfirmClick = { vm.confirm() },
        timerState = vm.timer.collectAsState(),
        onResendClick = { vm.resend() },
        onBackClick = { navController.navigateUp() },
        wrongConfirmationCode = vm.confirmationCodeIsWrong,
        scrollState = vm.scrollState,
    )
}

@Preview
@Composable
fun ConfirmationPagePreview() {
    KRSUTheme {
        ConfirmationPage(email = "ilimbeknarmatov@gmail.com")
    }
}

@Composable
fun ConfirmationPage(
    email: String,
    onWrongEmailClick: () -> Unit = {},
    confirmationCode: State<String> = mutableStateOf(""),
    onConfirmationChange: (String) -> Unit = {},
    onConfirmClick: () -> Unit = {},
    timerState: State<TimerState?> = mutableStateOf(null),
    onResendClick: () -> Unit = {},
    onBackClick: () -> Unit = {},
    wrongConfirmationCode: State<Boolean> = mutableStateOf(false),
    scrollState: ScrollState = rememberScrollState(),
) {
    Column(
        modifier = Modifier
            .fillMaxSize(),
    ) {
        BackArrow(
            modifier = Modifier.padding(
                top = 16.dp,
                start = 12.dp
            ),
            onClick = { onBackClick() },
        )
        Row(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
                .padding(top = 8.dp),
        ) {
            Spacer(modifier = Modifier.width(16.dp))
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .verticalScroll(scrollState),
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = R.string.confirmation_code.string,
                    style = MaterialTheme.typography.h5,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colors.onBackground,
                    modifier = Modifier
                        .fillMaxWidth(),
                )
                EmailWarning(email = email) {
                    onWrongEmailClick()
                }
                CodeConfirmTextField(
                    text = confirmationCode,
                    onChange = onConfirmationChange,
                    colors = MaterialTheme.colors.outlinedTextColors(),
                    isErrorTextVisible = true,
                    error = wrongConfirmationCode,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 20.dp),
                ) {
                    Text(text = R.string.wrong_confirmation_code.string)
                }
                AppGradientButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 12.dp),
                    onClick = onConfirmClick
                ) {
                    Text(text = R.string.confirm.string)
                }
            }
            Spacer(modifier = Modifier.width(16.dp))
        }
        val buttonColors = ButtonDefaults.buttonColors(
            disabledBackgroundColor = MaterialTheme.colors.primary.copy(
                alpha = 0.2f
            ),
            disabledContentColor = MaterialTheme.colors.onPrimary.copy(
                alpha = 0.8f
            ),
        )
        ButtonDefaults.ContentPadding
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp),
            enabled = (timerState.value is TimerState.Finished),
            onClick = onResendClick,
            colors = buttonColors,
            contentPadding = PaddingValues(
                horizontal = 16.dp,
                vertical = 14.dp
            ),
        ) {
            val titleAlign =
                if (timerState.value !is TimerState.TickString) {
                    TextAlign.Center
                } else TextAlign.Start
            Text(
                text = R.string.resend.string,
                modifier = Modifier.weight(1f),
                textAlign = titleAlign,
            )
            if (timerState.value is TimerState.TickString) {
                Text(
                    text = (timerState.value as? TimerState.TickString)?.time
                        ?: ""
                )
            }
        }
    }
}

@Preview
@Composable
fun EmailWaringPreview() {
    EmailWarning(email = "ilimbeknarmatov@gmail.com") {
    }
}

@Composable
private fun EmailWarning(
    email: String,
    onWrongEmailClick: () -> Unit
) {
    val annotatedText = buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                color = MaterialTheme.colors.onBackground.copy(
                    0.4f
                ),
            )
        ) {
            append("${R.string.code_sent_to.string} $email")
        }

        pushStringAnnotation(
            tag = "URL",
            annotation = ""
        )
        withStyle(
            style = SpanStyle(
                color = MaterialTheme.colors.primary,
            )
        ) {
            append(" " + R.string.wrong_email.string)
        }

        pop()
    }

    ClickableText(
        text = annotatedText,
        onClick = { offset ->
            annotatedText.getStringAnnotations(
                tag = "URL", start = offset,
                end = offset
            )
                .firstOrNull()?.let {
                    onWrongEmailClick()
                }
        }
    )
}
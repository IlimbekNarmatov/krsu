@file:OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalFoundationApi::class, ExperimentalMaterialApi::class,
    ExperimentalMaterialApi::class, ExperimentalFoundationApi::class
)

package com.ilimn.krsu.ui.main

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemsIndexed
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.ilimn.domain.entity.Link
import com.ilimn.domain.entity.Profession
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.navigatePopUp
import com.ilimn.krsu.ui.utils.string
import com.ilimn.krsu.vm.main.MainVM


@Composable
fun MainScreen(
    navController: NavController,
    vm: MainVM = viewModel(),
) {
    val context = LocalContext.current
    rememberSystemUiController().apply {
        setStatusBarColor(MaterialTheme.colors.background)
    }
    val professions = vm.getProfessionsPagination()
        ?.collectAsLazyPagingItems()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        Search(
            onTextChange = { vm.setSearchText(it) },
            onSearch = {},
            text = vm.searchText,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth(),
        )
        Spacer(modifier = Modifier.height(16.dp))
        GoToSite(
            link = Link(
                link = "https://krsu.edu.kg/",
                title = R.string.go_to_site.string,
            ),
            modifier = Modifier.padding(horizontal = 16.dp),
        ) {
            vm.openSite()
        }
        professions?.let {
            ProfessionsList(
                listState = vm.listState,
                items = it,
                onClick = {
                    vm.goToProfession(it.id)
                }
            )
        }
    }
    LaunchedEffect(Unit) {
        vm.navigation
            .collect { navigation ->
                when (navigation) {
                    is MainVM.Navigation.GotoSite -> {
                        context.startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://krsu.edu.kg/")
                            )
                        )
                    }
                    else -> {
                        navController.navigatePopUp(navigation)
                    }
                }
            }
    }
}

@Composable
private fun ProfessionsList(
    listState: LazyListState,
    items: LazyPagingItems<Profession>,
    onClick: (Profession) -> Unit,
) {
    LazyColumn(
        modifier = Modifier
            .padding(bottom = 56.dp)
            .fillMaxWidth(),
        state = if (items.itemSnapshotList.size > 0) {
            listState
        } else LazyListState(),
        contentPadding = PaddingValues(horizontal = 16.dp),
    ) {
        stickyHeader {
            Spacer(modifier = Modifier.height(16.dp))
        }
        itemsIndexed(
            items,
            key = { index, item -> item.id },
            itemContent = { index, profession ->
                profession?.let {
                    ProfessionItem(
                        profession = it,
                        listener = {
                            onClick(it)
                        }
                    )
                }
                Spacer(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp)
                        .height(1.dp)
                        .background(
                            MaterialTheme.colors.primary.copy(
                                alpha = 0.09f
                            )
                        )
                )
            },
        )
    }
}

private fun List<LoadState>.isAnyLoading(): Boolean =
    any { it is LoadState.Loading }

@ExperimentalMaterialApi
@Preview
@Composable
fun SearchPreview() {
    KRSUTheme {
        val searchText = remember { mutableStateOf("") }
        Search(
            text = searchText,
            onTextChange = { searchText.value = it },
            onSearch = {},
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@ExperimentalMaterialApi
@Composable
fun Search(
    modifier: Modifier = Modifier,
    text: State<String> = mutableStateOf(""),
    onTextChange: (String) -> Unit = {},
    onSearch: () -> Unit = {},
) {
    @Composable
    fun SearchIcon() {
        Card(
            modifier = Modifier.size(30.dp),
            onClick = { onSearch() },
            backgroundColor = Color.Transparent,
            elevation = 0.dp,
            shape = CircleShape
        ) {
            Box(contentAlignment = Alignment.Center) {
                Image(
                    painter = painterResource(id = R.drawable.ic_search),
                    contentDescription = stringResource(id = R.string.search)
                )
            }
        }
    }

    OutlinedTextField(
        value = text.value,
        onValueChange = { onTextChange(it) },
        trailingIcon = {
            SearchIcon()
        },
        modifier = modifier,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            backgroundColor = MaterialTheme.colors.primary.copy(alpha = 0.06f),
            unfocusedBorderColor = Color.Transparent
        ),
        keyboardActions = KeyboardActions(
            onSearch = { onSearch() },
        ),
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Search
        ),
    )
}

@ExperimentalMaterialApi
@Preview
@Composable
fun GoToSitePreview() {
    val link = Link(
        id = 0,
        link = "https://google.com",
        title = "Hello World",
    )
    KRSUTheme {
        GoToSite(link = link) {
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun GoToSite(
    modifier: Modifier = Modifier,
    link: Link,
    onClick: (Link) -> Unit = {},
) {
    var isIconLoading by remember { mutableStateOf(false) }
    val gradientColors = listOf(
        MaterialTheme.colors.primary,
        MaterialTheme.colors.primaryVariant,
    )
    Card(
        modifier = modifier,
        onClick = { onClick(link) },
        elevation = 0.dp,
        backgroundColor = Color.Transparent,
        indication = rememberRipple(color = MaterialTheme.colors.onPrimary)
    ) {
        Row(
            modifier = Modifier
                .background(
                    brush = Brush.horizontalGradient(
                        gradientColors,
                        startX = 0.8f
                    )
                )
                .padding(vertical = 20.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Box(contentAlignment = Alignment.Center) {
                if (isIconLoading) {
                    CircularProgressIndicator(
                        strokeWidth = 1.dp,
                        modifier = Modifier.size(
                            20.dp
                        )
                    )
                }
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(R.drawable.img_logo)
                        .crossfade(true)
                        .build(),
                    contentDescription = stringResource(R.string.profession),
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.size(24.dp),
                    onLoading = { isIconLoading = true },
                    onSuccess = { isIconLoading = false },
                    onError = { isIconLoading = false },
                )
            }
            Spacer(modifier = Modifier.width(12.dp))
            Text(
                text = link.title ?: "",
                modifier = Modifier.weight(1f),
                color = MaterialTheme.colors.onPrimary,
                style = MaterialTheme.typography.body2,
                fontWeight = FontWeight.Bold,
            )
            Spacer(modifier = Modifier.width(12.dp))
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = stringResource(
                    id = R.string.open
                ),
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onPrimary)
            )
            Spacer(modifier = Modifier.width(16.dp))
        }
    }
}

@ExperimentalMaterialApi
@Preview
@Composable
fun ProfessionItemPreview() {
    KRSUTheme {
        val profession = Profession(0, "Doctor")
        ProfessionItem(profession = profession, listener = {})
    }
}

@ExperimentalMaterialApi
@Composable
fun ProfessionItem(
    profession: Profession,
    listener: (Profession) -> Unit
) {
    Card(
        elevation = 0.dp,
        onClick = { listener(profession) },
        modifier = Modifier
            .fillMaxWidth(),
        backgroundColor = Color.Transparent,
        indication = rememberRipple(color = MaterialTheme.colors.primary)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Card(
                modifier = Modifier.size(36.dp),
                backgroundColor = MaterialTheme.colors.surface.copy(
                    alpha = 0.9f
                ),
                elevation = 0.dp
            ) {
                var isIconLoading by remember { mutableStateOf(false) }
                Box(contentAlignment = Alignment.Center) {
                    if (isIconLoading) {
                        CircularProgressIndicator(
                            strokeWidth = 1.dp,
                            modifier = Modifier.size(
                                20.dp
                            )
                        )
                    }
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current)
                            .data(profession.icon)
                            .crossfade(true)
                            .build(),
                        error = painterResource(R.drawable.ic_profession),
                        contentDescription = stringResource(R.string.profession),
                        contentScale = ContentScale.Crop,
                        modifier = Modifier.clip(CircleShape),
                        onLoading = { isIconLoading = true },
                        onSuccess = { isIconLoading = false },
                        onError = { isIconLoading = false },
                    )
                }
            }
            Spacer(modifier = Modifier.size(8.dp))
            Text(
                text = profession.name,
                modifier = Modifier.weight(1f)
            )
            Spacer(modifier = Modifier.size(8.dp))
            Image(
                painter = painterResource(id = R.drawable.ic_arrow),
                contentDescription = stringResource(id = R.string.open),
                colorFilter = ColorFilter.tint(
                    color = MaterialTheme.colors.onSurface
                        .copy(alpha = 0.5f)
                )
            )
        }
    }
}

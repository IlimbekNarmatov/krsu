package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetMessagesUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(
        page: Int,
        size: Int = 20,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ) = withContext(dispatcher) {
        repo.getMessages(page, size)
    }

}
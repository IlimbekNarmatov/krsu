package com.ilimn.domain.usecase

class ValidateEmailUC {

    operator fun invoke(email: String): Boolean {
        return email.isNotEmpty()
    }

}
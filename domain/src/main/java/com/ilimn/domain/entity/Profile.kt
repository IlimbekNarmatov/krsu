package com.ilimn.domain.entity

data class Profile(
    val firstName: String,
    val lastName: String,
    val citizenship: Citizenship,
    val school: School,
) {
    companion object {
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val CITIZENSHIP = "citizenship"
        const val SCHOOL = "school"
    }
}

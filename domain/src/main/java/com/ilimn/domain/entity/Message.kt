package com.ilimn.domain.entity


data class Message(
    val id: Int = 0,
    val message: String = "",
    val sentTime: Long? = null,
    val sentTimeText: String? = null,
    val isOwner: Boolean? = null, // do not use
    val messageType: MessageType,
    val dateText: String? = null,
)
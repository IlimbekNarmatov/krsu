@file:OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalFoundationApi::class
)

package com.ilimn.krsu.ui.faq

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemsIndexed
import com.ilimn.domain.entity.Faq
import com.ilimn.domain.util.toAnswer
import com.ilimn.domain.util.toQuestion
import com.ilimn.krsu.R
import com.ilimn.krsu.ui.theme.KRSUTheme
import com.ilimn.krsu.ui.utils.*
import com.ilimn.krsu.vm.faq.FaqVM

@Composable
fun FaqScreen(
    navController: NavController,
    vm: FaqVM = viewModel(),
) {
    LaunchedEffect(Unit) {
        vm.navigation.collect { navigation ->
            navController.navigatePopUp(navigation)
        }
    }
    val professions = vm.getFaqPagination()
        ?.collectAsLazyPagingItems()
    professions?.let {
        FaqPage(
            onExpandClick = { faq -> vm.toggleFaq(faq) },
            onWriteUsClick = { vm.gotoMessageUsClick() },
            faqState = vm.questionsState,
            items = it,
            expandedSet = vm.expandedQuestions,
        )
    }
}

@Composable
fun FaqPage(
    onNotificationClick: () -> Unit = {},
    onExpandClick: (Faq) -> Unit = {},
    onWriteUsClick: () -> Unit = {},
    faqState: LazyListState = LazyListState(),
    items: LazyPagingItems<Faq>,
    expandedSet: State<Set<Int>> = mutableStateOf(emptySet())
) {
    LazyColumn(
        state = faqState,
        modifier = Modifier
            .padding(bottom = 56.dp)
            .fillMaxSize(),
        contentPadding = PaddingValues(
            start = 16.dp,
            end = 16.dp
        ),
    ) {
        item {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = R.string.faq_headline.string,
                    modifier = Modifier.weight(1f),
                    style = MaterialTheme.typography.h5.copy(
                        fontWeight = FontWeight.Bold
                    ),
                )
                NotificationComposable { onNotificationClick() }
            }
        }
        item {
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp)
                    .background(
                        MaterialTheme.colors.onBackground.copy(
                            alpha = 0.04f
                        )
                    )
            )
        }
        item {
            Column(modifier = Modifier.fillMaxWidth()) {
                Spacer(modifier = Modifier.height(16.dp))
                WriteSupport { onWriteUsClick() }
            }
        }
        itemsIndexed(items = items) { i, faq ->
            faq?.let {
                Spacer(modifier = Modifier.height(8.dp))
                ExpandableTextCard(
                    question = it.question.toQuestion(),
                    answer = it.answer.toAnswer(),
                    isAnswerExpanded = expandedSet.value.contains(
                        it.id
                    ),
                    onQuestionClick = { question ->
                        onExpandClick(
                            it
                        )
                    },
                    onAnswerClick = { answer ->
                        onExpandClick(
                            it
                        )
                    },
                )
                if (i == items.itemCount - 1) {
                    Spacer(modifier = Modifier.height(8.dp))
                }
            }
        }
    }
}

@ExperimentalMaterialApi
@Preview
@Composable
fun WriteSupportPreview() {
    KRSUTheme {
        WriteSupport()
    }
}

@ExperimentalMaterialApi
@Composable
fun WriteSupport(
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {},
) {
    val gradientColors = listOf(
        MaterialTheme.colors.primary,
        MaterialTheme.colors.primaryVariant,
    )
    Card(
        modifier = modifier,
        onClick = { onClick() },
        shape = RoundedCornerShape(
            topStart = 8.dp,
            topEnd = 8.dp,
            bottomEnd = 8.dp,
        ),
        elevation = 0.dp,
        backgroundColor = Color.Transparent,
        indication = rememberRipple(color = MaterialTheme.colors.onPrimary)
    ) {
        Row(
            modifier = Modifier
                .background(
                    brush = Brush.horizontalGradient(
                        gradientColors,
                        startX = 0.8f
                    )
                )
                .padding(
                    start = 12.dp,
                    end = 18.dp,
                    top = 12.dp,
                    bottom = 12.dp
                ),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            WriteSupportIcon { onClick() }
            Spacer(modifier = Modifier.width(12.dp))
            Column(modifier = Modifier.weight(1f)) {
                Text(
                    text = R.string.write_support.string,
                    style = MaterialTheme.typography.subtitle1,
                    color = MaterialTheme.colors.onPrimary,
                )
                Text(
                    text = R.string.answer_as_soon_as_possible.string,
                    style = MaterialTheme.typography.overline,
                    color = MaterialTheme.colors.onPrimary,
                )
            }
            IconButton(onClick = { onClick() }) {
                Image(
                    painter = R.drawable.ic_arrow.painter,
                    contentDescription = R.string.write_support.string,
                    colorFilter = ColorFilter.tint(MaterialTheme.colors.onPrimary),
                )
            }
        }
    }
}

@Preview
@Composable
private fun WriteSupportIconPreview() {
    KRSUTheme {
        WriteSupportIcon()
    }
}

@Composable
private fun WriteSupportIcon(onClick: () -> Unit = {}) {
    IconButton(
        onClick = { onClick() },
        modifier = Modifier
            .clip(RoundedCornerShape(8.dp))
            .size(48.dp)
            .background(
                MaterialTheme.colors.background
            ),
    ) {
        Image(
            painter = R.drawable.ic_write_support.painter,
            contentDescription = R.string.write_support.string,
        )
    }
}

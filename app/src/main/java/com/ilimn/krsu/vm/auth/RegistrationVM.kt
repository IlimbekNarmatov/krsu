package com.ilimn.krsu.vm.auth

import androidx.compose.foundation.ScrollState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.ilimn.domain.entity.Login
import com.ilimn.domain.usecase.SignUpUC
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.ui.navigation.CONFIRM_SCREEN_NO_ARG
import com.ilimn.krsu.vm.util.BaseVM
import com.ilimn.krsu.vm.util.NavigationProperty
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class RegistrationVM(
    private val signUpUC: SignUpUC,
) : BaseVM() {

    private val _navigation = MutableSharedFlow<Navigation>()
    val navigation: SharedFlow<Navigation> = _navigation
    private val _email = mutableStateOf("")
    val email: State<String> = _email
    private val _password = mutableStateOf("")
    val password: State<String> = _password
    private val _emailError = mutableStateOf(false)
    val emailError: State<Boolean> = _emailError
    private val _passwordError = mutableStateOf(false)
    val passwordError: State<Boolean> = _passwordError
    val pageScrollState = ScrollState(0)

    fun setEmail(email: String) {
        _email.value = email
        _emailError.value = false
    }

    fun setPassword(password: String) {
        _password.value = password
        _passwordError.value = false
    }

    fun signUp() {
        viewModelScope.launch {
            signUpUC(
                mapOf(
                    Login.Email.EMAIL to email.value,
                    Login.Email.PASSWORD to password.value,
                )
            ).onSuccessSuspend {
                _navigation.emit(
                    Navigation.GotoConfirmation(email.value)
                )
            }.onFailureSuspend {
                if (it is Failure.Format) {
                    _emailError.value =
                        it.type.contains(Login.Email.EMAIL)
                    _passwordError.value =
                        it.type.contains(Login.Email.PASSWORD)
                    return@onFailureSuspend
                }
                _failure.send(it)
            }
        }
    }

    sealed class Navigation : NavigationProperty {
        data class GotoConfirmation(
            val email: String
        ) : Navigation() {
            override val route: String = CONFIRM_SCREEN_NO_ARG + email
        }
    }

}
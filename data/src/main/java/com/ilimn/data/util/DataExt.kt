package com.ilimn.data.util

import android.accounts.NetworkErrorException
import com.ilimn.domain.util.Failure

internal fun String.cleanPhoneNumber() =
    replace("[^+\\d]".toRegex(), "")

internal fun Failure.getException() = when (this) {
    is Failure.Network -> NetworkErrorException()
    is Failure.Server -> NetworkErrorException()
    is Failure.Util -> Exception(this.message)
    is Failure.Default -> Exception()
    is Failure.Parse -> Exception("parse error")
    is Failure.Format -> Exception("format error")
}

//internal fun Uri.toMultiPart(name: String): MultipartBody.Part {
//    val file = File(path ?: "")
//    val requestFile =
//        file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
//    return MultipartBody.Part.createFormData(
//        name,
//        file.name,
//        requestFile
//    )
//}

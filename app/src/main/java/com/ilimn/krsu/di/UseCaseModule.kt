package com.ilimn.krsu.di

import com.ilimn.domain.usecase.*
import org.koin.dsl.module

val useCaseModule = module {
    factory { GetVersionUC(get()) }
    factory { GetIsOnBoardingSeenUC(get()) }
    factory { GetOnBoardingsUC(get()) }
    factory { OnBoardingSeenUC(get()) }
    factory { GetProfessionsUC(get()) }
    factory { GetDetailProfessionUC(get()) }
    factory { IsLoggedInUC(get()) }
    factory { GetFaqsUC(get()) }
    factory { SignInUC(get()) }
    factory { SignUpUC(get()) }
    factory { TimerUC() }
    factory { FormattedTimerUC() }
    factory { ConfirmUC(get()) }
    factory { GetProfileUC(get(), get()) }
    factory { UpdateProfileUC(get(), get()) }
    factory { GetCountriesUC(get()) }
    factory { GetMessagesPagingUC(get()) }
    factory { GetMessagesUC(get()) }
    factory { SendMessageUC(get()) }
}

package com.ilimn.krsu.vm.start

import androidx.lifecycle.viewModelScope
import com.ilimn.domain.entity.Version
import com.ilimn.domain.usecase.BaseUseCase
import com.ilimn.domain.usecase.GetIsOnBoardingSeenUC
import com.ilimn.domain.usecase.GetVersionUC
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.BuildConfig
import com.ilimn.krsu.ui.navigation.MAIN_SCREEN
import com.ilimn.krsu.ui.navigation.ON_BOARDING_SCREEN
import com.ilimn.krsu.ui.navigation.SPLASH_SCREEN
import com.ilimn.krsu.vm.util.BaseVM
import com.ilimn.krsu.vm.util.NavigationProperty
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class SplashVM(
    private val getVersionUC: GetVersionUC,
    private val getIsOnBoardingSeenUC: GetIsOnBoardingSeenUC,
) : BaseVM() {

    private val _navigation = MutableSharedFlow<Action.Navigation>()
    val navigation: SharedFlow<Action.Navigation> = _navigation
    private var delay = MutableStateFlow(false)
    private var version = MutableStateFlow<Version?>(null)
    private var isOnBoardingSeen = MutableStateFlow<Boolean?>(null)
    private val mediator = combine(
        flow = delay,
        flow2 = version,
        flow3 = isOnBoardingSeen
    ) { _, _, _ ->
        if (!isAllParametersValid()) return@combine
        val checkVersion = checkVersion(version.value!!)
        when {
            checkVersion != null -> {
                _navigation.emit(checkVersion)
            }
            isOnBoardingSeen.value == false -> {
                _navigation.emit(Action.Navigation.GotoOnBoarding)
            }
            else -> {
                _navigation.emit(Action.Navigation.GotoMain)
            }
        }
    }

    private fun isAllParametersValid() =
        delay.value && version.value != null
                && isOnBoardingSeen.value != null

    private fun checkVersion(
        version: Version
    ): Action.Navigation.GotoVersion? = if (
        BuildConfig.VERSION_NAME == version.name
    ) null else Action.Navigation.GotoVersion(version)

    init {
        viewModelScope.launch {
            delay(SPLASH_DELAY)
            delay.value = true
        }
        viewModelScope.launch {
            delay(1_000)
            getVersionUC(BaseUseCase.None)
                .onSuccessSuspend { version.emit(it) }
                .onFailureSuspend { _failure.send(it) }
        }
        viewModelScope.launch {
            delay(1_500)
            getIsOnBoardingSeenUC(BaseUseCase.None)
                .onSuccessSuspend { isOnBoardingSeen.emit(it) }
                .onFailureSuspend { _failure.send(it) }
        }
        viewModelScope.launch { mediator.collect() }
    }

    sealed class Action {
        sealed class Navigation : NavigationProperty {
            object GotoMain : Navigation() {
                override val route: String = MAIN_SCREEN
                override val popUpRoute = SPLASH_SCREEN
                override val isInclusive: Boolean = true
            }

            object GotoOnBoarding : Navigation() {
                override val route: String = ON_BOARDING_SCREEN
                override val popUpRoute = SPLASH_SCREEN
                override val isInclusive: Boolean = true
            }

            data class GotoVersion(
                val version: Version
            ) : Navigation()
        }

        data class State(
            val version: Version? = null,
        ) : Action()
    }

    companion object {
        const val SPLASH_DELAY = 2_000L
    }

}
package com.ilimn.domain.entity

data class DetailProfession(
    val id: Int,
    val title: String,
    val description: String,
    val images: List<String> = emptyList(),
    val title2: String = "",
    val applying: List<ApplyingDescription> = emptyList(),
)

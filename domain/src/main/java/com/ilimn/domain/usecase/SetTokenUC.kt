package com.ilimn.domain.usecase

import com.ilimn.domain.entity.Token
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import com.ilimn.domain.util.Success
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SetTokenUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(
        token: Token,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, Success> = withContext(dispatcher) {
        repo.setToken(token)
    }

}
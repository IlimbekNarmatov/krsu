package com.ilimn.domain.usecase

import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure

class IsLoggedInUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(): Either<Failure, Boolean> {
        return repo.isLoggedIn()
    }

}
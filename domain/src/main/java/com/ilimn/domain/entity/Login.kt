package com.ilimn.domain.entity

sealed class Login {
    data class Email(
        val email: String, val password: String
    ) : Login() {
        companion object {
            const val EMAIL = "email"
            const val PASSWORD = "password"
        }
    }

    data class Phone(
        val phoneNumber: String
    ) : Login()
}

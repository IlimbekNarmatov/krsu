package com.ilimn.domain.entity

sealed class Confirm {
    data class Email(
        val email: String, val confirmCode: String
    ) : Confirm()
}

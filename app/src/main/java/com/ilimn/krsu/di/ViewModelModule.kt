package com.ilimn.krsu.di

import com.ilimn.krsu.vm.auth.ConfirmScreenVM
import com.ilimn.krsu.vm.auth.LoginVM
import com.ilimn.krsu.vm.auth.RegistrationVM
import com.ilimn.krsu.vm.faq.AskQuestionVM
import com.ilimn.krsu.vm.faq.FaqVM
import com.ilimn.krsu.vm.main.MainVM
import com.ilimn.krsu.vm.main.ProfessionVM
import com.ilimn.krsu.vm.profile.ProfileVM
import com.ilimn.krsu.vm.start.OnBoardingVM
import com.ilimn.krsu.vm.start.SplashVM
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SplashVM(get(), get()) }
    viewModel { OnBoardingVM(get(), get()) }
    viewModel { MainVM(get()) }
    viewModel { ProfessionVM(get()) }
    viewModel { FaqVM(get(), get()) }
    viewModel { LoginVM(get()) }
    viewModel { RegistrationVM(get()) }
    viewModel {
        ConfirmScreenVM(get(), get(), get())
    }
    viewModel { ProfileVM(get(), get(), get()) }
    viewModel { AskQuestionVM(get(), get()) }
}

package com.ilimn.krsu.vm.faq

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.ilimn.domain.entity.Message
import com.ilimn.domain.entity.MyMessage
import com.ilimn.domain.usecase.GetMessagesPagingUC
import com.ilimn.domain.usecase.SendMessageUC
import com.ilimn.domain.util.onFailureSuspend
import com.ilimn.domain.util.onSuccessSuspend
import com.ilimn.krsu.vm.util.BaseVM
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class AskQuestionVM(
    private val getMessagesPagingUC: GetMessagesPagingUC,
    private val sendMessageUC: SendMessageUC,
) : BaseVM() {

    private val _action = MutableSharedFlow<Action>()
    val action: SharedFlow<Action> = _action
    private val _isLoading = mutableStateOf(false)
    val isLoading: State<Boolean> = _isLoading
    private var messages: Flow<PagingData<Message>>? = null

    val messagesScrollState = LazyListState()
    private val _message = mutableStateOf("")
    val message: State<String> = _message

    fun setMessage(text: String) {
        _message.value = text
    }

    fun sendMessage() {
        viewModelScope.launch {
            sendMessageUC(
                Message(
                    message = message.value,
                    messageType = MyMessage.Text
                )
            ).onSuccessSuspend {
                viewModelScope.launch {
                    _action.emit(Action.Refresh)
                }
                _message.value = ""
            }.onFailureSuspend { _failure.send(it) }
        }
    }

    fun getMessagesPagination(): Flow<PagingData<Message>>? {
        if (messages != null) {
            return messages
        }
        val paging: Flow<PagingData<Message>> = Pager(
            PagingConfig(
                pageSize = 10,
            ),
            pagingSourceFactory = {
                getMessagesPagingUC() as PagingSource<Int, Message>
            }
        ).flow.cachedIn(viewModelScope)
        messages = paging
        return paging
    }

    sealed class Action {
        object Refresh : Action()
    }

}
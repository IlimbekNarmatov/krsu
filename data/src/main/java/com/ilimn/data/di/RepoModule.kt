package com.ilimn.data.di

import com.ilimn.data.repoimpl.AppRepoImpl
import com.ilimn.data.repoimpl.DumpDataSource
import com.ilimn.domain.repository.AppRepo
import org.koin.dsl.module

val repoModule = module {
    single<AppRepo> { AppRepoImpl(get<DumpDataSource>()) }
}
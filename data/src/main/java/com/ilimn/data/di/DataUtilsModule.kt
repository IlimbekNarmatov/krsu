package com.ilimn.data.di

import com.ilimn.data.repoimpl.DumpDataSource
import org.koin.dsl.module

val dataUtilsModule = module {
    single { DumpDataSource() }
}
package com.ilimn.domain.entity

data class Profession(
    val id: Int,
    val name: String,
    val icon: String? = null
)

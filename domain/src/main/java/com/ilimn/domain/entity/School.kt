package com.ilimn.domain.entity

data class School(val name: String) {
    companion object {
        fun empty() = School("")
    }
}

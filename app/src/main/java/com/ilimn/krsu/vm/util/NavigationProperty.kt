package com.ilimn.krsu.vm.util

interface NavigationProperty {
    val route: String
        get() = ""
    val popUpRoute: String?
        get() = null
    val isInclusive: Boolean
        get() = false
    val isPopBackStack: Boolean
        get() = false
}

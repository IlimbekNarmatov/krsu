package com.ilimn.domain.usecase

import com.ilimn.domain.entity.DetailProfession
import com.ilimn.domain.repository.AppRepo
import com.ilimn.domain.util.Either
import com.ilimn.domain.util.Failure
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDetailProfessionUC(
    private val repo: AppRepo
) {

    suspend operator fun invoke(
        id: Int,
        dispatcher: CoroutineDispatcher = Dispatchers.Default,
    ): Either<Failure, DetailProfession> =
        withContext(dispatcher) {
            repo.getDetailProfession(id)
        }

}
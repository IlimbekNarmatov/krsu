package com.ilimn.domain.entity

data class Applying(
    val descriptions: List<ApplyingDescription>,
    val images: List<String>
)

data class ApplyingDescription(
    val id: Int,
    val title: String,
    val description: String
)

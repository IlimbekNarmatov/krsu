package com.ilimn.domain.entity

data class Version(
    val name: String,
    val isForceRefresh: Boolean
)